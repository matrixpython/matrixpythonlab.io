---
title: "{{ replace .Name "-" " " | title }}"
pronouns: ""
description: ""
lead: ""

date: {{ .Date }}
lastmod: {{ .Date }}

images: ["{{ .Name | urlize }}.jpg"]
avatar: ""

matrix_identifier: ""
matrix_username: ""

email_address: ""
gitlab_username: ""
github_username: ""
gitea_profile_url: ""
mastodon_profile_url: ""
website_url: ""

bot: false
matrix_moderator: flase
website_content: false
website_developer: false

draft: true
---
