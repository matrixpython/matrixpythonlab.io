---
title: "Join Spam Attack Update 5 (Final Update)"
description: ""
lead: '"spambot123 and 35538 others were removed" - Back to our former Glory!'
date: 2022-05-19T01:24:00+02:00
lastmod: 2022-05-19T01:24:00+02:00
draft: false
weight: 50
contributors: ["Michael Sasser"]
categories: ["Community", "Matrix"]
tags: ["Announcement"]
images: []
header_image: ""
floating_image: ""
floating_image_width: 40
floating_image_position: "right"
floating_image_caption: ""
---

Finally, the main Python room is back to where it was before the attack
happened. It took a little over 2 hours to ban the rest of the spam accounts,
with a heavily upgraded homeserver and without rate-limiting. But not only
users starting with `spambot` were removed. We did some analysis-magic and
found 18126 additional user accounts with a random appearing matrix user
identifier. All together, we removed 35.538 from the main Python room.

After that, It took about 24 hours to sync the bans to > 400 homeservers (not
including the banned ones).

## Banned servers

During our investigation, we banned following 54 homeservers permanently, which
enabled the attack in the first place. If you are a user on one of those
servers, you will not be able to join our community. If you are the
homeserver owner of one of those banned servers, and you want your server
unbanned, please [contact]({{< ref "contact" >}}) us.

Server list:

- `adfontesapertas.de`
- `amosb.selfhost.co`
- `ampextech.ddns.net`
- `aristillus.xyz`
- `atlmesh.net`
- `awesemble.de`
- `cactus.chat`
- `citadel7.org`
- `climatestrike.ch`
- `gcsg.zitech.cyou`
- `jlscode.com`
- `longtimeno.cyou`
- `m.ip-t.com`
- `matrix.chacah.io`
- `matrix.dp15.us`
- `matrix.fjolstad.no`
- `matrix.foxears.life`
- `matrix.geonat.nz`
- `matrix.gorghul.de`
- `matrix.gyro-tech.net`
- `matrix.jibby.org`
- `matrix.jonsweb.io`
- `matrix.kanela.cz`
- `matrix.koenhendriks.nl`
- `matrix.lat3st.de`
- `matrix.ln404.ru`
- `matrix.lorenzo12floxy.ga`
- `matrix.lyberry.com`
- `matrix.m4geek.com`
- `matrix.mms.lol`
- `matrix.moc.network`
- `matrix.mxroute.com`
- `matrix.nett.media`
- `matrix.pangrand.fr`
- `matrix.pclovers.su`
- `matrix.portl.live`
- `matrix.ring-0.net`
- `matrix.rong.zone`
- `matrix.spectreos.de`
- `matrix.sympa.city`
- `matrix.trustbtc.org`
- `matrix.verstehbahnhof.de`
- `matrix.x1101.net`
- `merrymount.family`
- `mtrx.9v5.de`
- `nang.zom.im`
- `neo.keanu.im`
- `nomadserver.us`
- `nsaspecialagents.chickenkiller.com`
- `rankenste.in`
- `renaissancemen.xyz`
- `vanderpot.net`
- `vergeylen.eu`
- `ziguana.club`
