---
title: 'The "📞 Call back"-Attack'
description: ""
lead: "About a call that ended in an amplification attack"
date: 2023-05-06T10:00:00+02:00
lastmod: 2023-05-06T10:00:00+02:00
draft: false
weight: 50
contributors: ["Michael Sasser"]
categories: ["Community", "Matrix"]
tags: ["Announcement"]
images: ["call_back_android.jpg", "events_without_legend.png"]
header_image: ""
floating_image: "call_back_android.jpg"
floating_image_width: 30
floating_image_position: "right"
floating_image_caption: ""
---

Many probably have noticed relatively quickly that something went horrible
wrong on May 06, 2023 around 08 AM (UTC), when _the Python room_ made a call
to all of it's users and hundreds of `📞 Call back` widgets popped up
in seconds.

So, what happened? At first, everything started out like a normal day in
Matrix. Then a few messages popped up:
"Uhh", "There's 12k people here", "wtf", "?", "Can we not?",
"Someone pressed the wrong button?".<br />
What happened was that five users created
a specific event in the Python room {{< identifier "#python:matrix.org" >}}
that lead to hundreds of calls to emerge, _from the room_.
In just a few minutes 85 Users left the room and the chaos widened.
Though, whether you have seen the actual calls or just the messages
of the perplexed users, depends on your client.<br />
We immediately started investigating the incident. When we noticed that what
ever happened continued, and servers of community members where crashing we
immediately closed down all of our rooms with the message
"This room is temporarily closed."
and continued with our investigation. It was also at that moment when we
really became aware of the magnitude of the attack. It took roughly 45
minutes from changing the permissions until the room was visible closed down.
After the load on our servers went down, we re-opened our rooms again later
in the evening.

If you like to see a visual representation of what happened, check out the
graph below. It shows the events in the room in different colors depending on
their type during the attack.

{{< img src="events_without_legend.png" height=800 caption="<em>Events in Python room stacked in 30s windows during the attack (time in UTC, without legend)</em>" >}}

Since the incident is related to a not yet publicly disclosed matrix security
issue _NVT#1548355_, we cannot go into more detail about what happened and how
users were instrumented to amplified the attack.
At least not yet. But we will, after the bug has been fixed and
an appropriate time has passed so users can update.

With a little help, we found a solution to protect our community rooms
from this attack for the future. But nevertheless, you might see two things
still happening as an aftermath:

- If you get a call from _the Python room_, please reject the call (it doesn't happen anything bad if you try to answer the call. You just get an error message).
- If you see a widget telling you, you missed a call, please ignore it.

After re-investigating and re-creating the incident later on, we found out
that in four of the five cases the possibility exists, that those users accidentally created
this kind of event and that a scenario with only one attacker was more plausible.
The reason for that is that the event the attacker initially created made it
possible for the others to create additional events of the same kind, by accident.

Since there was no sufficient evidence that they had the desire to cause harm,
there was no reason for them to be banned from the community anymore.

For the four users we banned during the attack, who have been involved
by accident, we would like to formally apologize for banning them. They
have been notified and we would appreciate, if they still want to come back.

<!-- markdownlint-disable-next-line MD036 -->
_🤙 The moderation Team 🤙_
