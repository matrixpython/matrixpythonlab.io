---
title: Community Space Promotion
description: |
  Let's talk about why we promote our community space on a weekly basis in the
  Python room
lead: |
  Let's talk about why we promote our community space on a weekly basis in the
  Python room
date: 2023-07-28T09:57:52+02:00
lastmod: 2023-08-02T10:58:09+02:00
draft: false
weight: 50
contributors: ["Michael Sasser"]
categories: ["Community", "Matrix"]
tags: ["Announcement"]
images: ["message_in_python_room.png"]
header_image: ""
floating_image: "message_in_python_room.png"
floating_image_width: 40
floating_image_position: "right"
floating_image_caption: "Promotion message in the Python room"
---

In the Python room, you might have noticed that every <del>Monday around
10 AM (UTC)</del> Sunday around 10 PM (UTC), we started to promoting the
Python community Space. If you've ever wondered why we do this, let me take
you back to the origins of the project in December 2020.

Back then, there was a feature called "Communities" that shared some
similarities with Spaces, but it had its limitations and wasn't widely used
by users. As it was the only available option for grouping rooms at the
time, we decided against using it.

When Spaces were developed and became stable, we saw an
opportunity to create a better experience for our community. We took the leap
and created our very first Space, the
{{< identifier "#python-community:matrix.org" >}} Space, which became home
to the Python room {{< identifier "#python:matrix.org" >}} and two new rooms:
{{< identifier "#python-meta:matrix.org" >}} for suggestions and proposals,
and {{< identifier "#python-off-topic:matrix.org" >}} for everything off-topic.

At the time, not all clients supported Spaces, so we planned
to gradually introduce it. Our hope was that by adding those rooms and the
Space, we would encourage a smooth transition. This way, users would
naturally start to shift towards using the Space and its newly created rooms.

We understand that this can take time to be adopted,
and our approach was to encourage adoption organically rather than forcing an
immediate change. However, as support for Spaces became more widespread we
hoped users would adopt the space more and thereby also discover the
new rooms, so joins for the majority of users would look more like shown below.

<div class="text-center"
  {{< figure caption="How we intended users joining the our rooms" >}}
    {{< graphviz "python_space_transition_join_graph" >}}
  {{< /figure >}}
</div>

Users whose clients do not support Spaces are still able to join the
individual rooms they are interested in directly. On the other hand, users
with clients that do support Spaces have the option to join the Space first
and then join to the specific rooms they wish to participate in.
In reality, the majority of users tend to follow a different pattern:

<div class="text-center"
  {{< figure caption="How most users in reality join our rooms" >}}
    {{< graphviz "python_community_space" >}}
  {{< /figure >}}
</div>

The reason behind this behavior is quite straightforward. When users search
the room directory for anything related to "Python", the Python room naturally
appears as the top result in the roomlist. This is due to the
significant number of users who have joined the Python room. Our
other rooms and the community Space are positioned further down the list and
do not appear as interesting.

<div class="text-center"
  {{< figure caption="Comparison of joined users per room" >}}
    {{< chart height=250 >}}
    {
        type: 'doughnut',
        data: {
            labels: ['Python', 'Community', 'Off-Topic', 'Meta', 'Space'],
            datasets: [{
                data: [13179, 484, 262, 142, 23],
                backgroundColor: [
                    'rgba(255, 99, 132, 0.2)',
                    'rgba(54, 162, 235, 0.2)',
                    'rgba(255, 206, 86, 0.2)',
                    'rgba(75, 192, 192, 0.2)',
                    'rgba(153, 102, 255, 0.2)',
                ],
                borderColor: [
                    'rgba(255, 99, 132, 1)',
                    'rgba(54, 162, 235, 1)',
                    'rgba(255, 206, 86, 1)',
                    'rgba(75, 192, 192, 1)',
                    'rgba(153, 102, 255, 1)',
                ],
            }]
        },
        options: {
            responsive: true,
            maintainAspectRatio: false,
            plugins: {legend: {position: 'right'}}
        }
    }
    {{< /chart >}}
  {{< /figure >}}
</div>

One significant challenge that arises from this situation is that new users,
in particular, may not be aware of the existence of our other rooms, which often
leads to off-topic conversations in the Python room. Due to the linear nature
of most chats, these off-topic discussions push up legitimate Python-related
questions. As a result, those questions may not receive attention and stay
unanswered.

Another concern is that our other rooms have not experienced as much activity
as we had hoped for. Over the past six months, the Python room saw an average
of 49.18 messages per day. However, in contrast, the off-topic room only
saw an average of 6.28 messages per day, and the meta room received just
0.27 messages per day on average.

While addressing the main issue may require more drastic changes, like making
the space mandatory, which comes with it's own set of complications,
fostering more engagement in the off-topic room should not require such
extremes. Promoting the Python community Space alone may not be a
complete remedy. Nonetheless, it can serve as a positive step towards improving
the overall community experience.

The chart displayed below illustrates the number of joined members in the
community Space since it was created.

<div class="text-center"
  {{< figure caption="Members in the Community Space since the space was created (resampled to 98 datapoints)" >}}
    {{< chart >}}
    {
        type: 'line',
        data: {
            datasets: [{
                label: 'Joined Members',
                lineTension: 0.2,
                pointStyle: false,
                data: [
                     {x: 1632948554190, y: 0}, {x: 1634428800000, y: 83},
                     {x: 1635033600000, y: 108}, {x: 1635638400000, y: 111},
                     {x: 1636243200000, y: 116}, {x: 1636848000000, y: 120},
                     {x: 1637452800000, y: 122}, {x: 1638057600000, y: 124},
                     {x: 1638662400000, y: 125}, {x: 1639267200000, y: 127},
                     {x: 1639872000000, y: 136}, {x: 1640476800000, y: 140},
                     {x: 1641081600000, y: 142}, {x: 1641686400000, y: 146},
                     {x: 1642291200000, y: 148}, {x: 1642896000000, y: 150},
                     {x: 1643500800000, y: 156}, {x: 1644105600000, y: 160},
                     {x: 1644710400000, y: 160}, {x: 1645315200000, y: 160},
                     {x: 1645920000000, y: 162}, {x: 1646524800000, y: 166},
                     {x: 1647129600000, y: 167}, {x: 1647734400000, y: 171},
                     {x: 1648339200000, y: 175}, {x: 1648944000000, y: 178},
                     {x: 1649548800000, y: 184}, {x: 1650153600000, y: 185},
                     {x: 1650758400000, y: 188}, {x: 1651363200000, y: 190},
                     {x: 1651968000000, y: 198}, {x: 1652572800000, y: 209},
                     {x: 1653177600000, y: 214}, {x: 1653782400000, y: 216},
                     {x: 1654387200000, y: 221}, {x: 1654992000000, y: 220},
                     {x: 1655596800000, y: 221}, {x: 1656201600000, y: 226},
                     {x: 1656806400000, y: 232}, {x: 1657411200000, y: 237},
                     {x: 1658016000000, y: 242}, {x: 1658620800000, y: 244},
                     {x: 1659225600000, y: 250}, {x: 1659830400000, y: 256},
                     {x: 1660435200000, y: 258}, {x: 1661040000000, y: 258},
                     {x: 1661644800000, y: 259}, {x: 1662249600000, y: 262},
                     {x: 1662854400000, y: 265}, {x: 1663459200000, y: 275},
                     {x: 1664064000000, y: 279}, {x: 1664668800000, y: 282},
                     {x: 1665273600000, y: 287}, {x: 1665878400000, y: 292},
                     {x: 1666483200000, y: 294}, {x: 1667088000000, y: 295},
                     {x: 1667692800000, y: 299}, {x: 1668297600000, y: 306},
                     {x: 1668902400000, y: 311}, {x: 1669507200000, y: 318},
                     {x: 1670112000000, y: 320}, {x: 1670716800000, y: 321},
                     {x: 1671321600000, y: 329}, {x: 1671926400000, y: 329},
                     {x: 1672531200000, y: 331}, {x: 1673136000000, y: 334},
                     {x: 1673740800000, y: 334}, {x: 1674345600000, y: 336},
                     {x: 1674950400000, y: 335}, {x: 1675555200000, y: 335},
                     {x: 1676160000000, y: 336}, {x: 1676764800000, y: 338},
                     {x: 1677369600000, y: 337}, {x: 1677974400000, y: 340},
                     {x: 1678579200000, y: 343}, {x: 1679184000000, y: 350},
                     {x: 1679788800000, y: 357}, {x: 1680393600000, y: 359},
                     {x: 1680998400000, y: 361}, {x: 1681603200000, y: 364},
                     {x: 1682208000000, y: 367}, {x: 1682812800000, y: 373},
                     {x: 1683417600000, y: 376}, {x: 1684022400000, y: 381},
                     {x: 1684627200000, y: 382}, {x: 1685232000000, y: 385},
                     {x: 1685836800000, y: 387}, {x: 1686441600000, y: 389},
                     {x: 1687046400000, y: 393}, {x: 1687651200000, y: 400},
                     {x: 1688256000000, y: 400}, {x: 1688860800000, y: 400},
                     {x: 1689465600000, y: 414}, {x: 1690070400000, y: 450},
                     {x: 1690675200000, y: 459},
            ],
                backgroundColor: [
                    'rgba(255, 99, 132, 0.2)',
                ],
                borderColor: [
                    'rgba(255, 99, 132, 1)',
                ],
                borderWidth: 1
            }]
        },
        options: {
          maintainAspectRatio: false,
          scales: {
            x: {
              type: 'time',
              time: {
                unit: 'month',
                displayFormats: {
                    month: 'YYYY-MM'
                },
              tooltipFormat: 'YYYY-MM-DD'
              },
              title: {
                display: true,
                text: 'Date'
              }
            },
            y: {
              title: {
                display: true,
                text: 'Members'
              }
            }
          }
        }
    }
    {{< /chart >}}
  {{< /figure >}}
</div>

Initially, there is a sharp increase in joined members, which can be
attributed to the initial announcement of the Space. Subsequently, the pattern
follows a relatively linear trajectory, similar to what we observe with the
other rooms.

An interesting phase stands out between December 2022 and March 2023,
where we experienced a shallower slope. This trend is not exclusive to the
community Space; it is also evident in the meta room and, to a lesser extent,
in the off-topic room. Interestingly, the slowdown in joins in the
off-topic room started one month later compared to the meta room and the
community Space.

Now, let's see how the promotion of the community Space has impacted our
community thus far. To gain deeper insights into when the promotions started
we have prepared a more detailed chart below.

<div class="text-center"
  {{< figure caption="Members in the Community Space (full resolution)" >}}
    {{< chart >}}
    {
        type: 'line',
        data: {
            datasets: [{
                label: 'Joined Members',
                lineTension: 0.2,
                pointStyle: false,
                data: [
                    {x: 1688479200000, y: 398}, {x: 1688508000000, y: 399},
                    {x: 1688529600000, y: 400}, {x: 1688680800000, y: 399},
                    {x: 1688691600000, y: 400}, {x: 1688713200000, y: 401},
                    {x: 1688734800000, y: 402}, {x: 1688738400000, y: 401},
                    {x: 1688742000000, y: 402}, {x: 1688821200000, y: 401},
                    {x: 1688850000000, y: 400}, {x: 1688929200000, y: 400},
                    {x: 1688986800000, y: 399}, {x: 1689091200000, y: 403},
                    {x: 1689094800000, y: 409}, {x: 1689098400000, y: 411},
                    {x: 1689102000000, y: 412}, {x: 1689112800000, y: 413},
                    {x: 1689138000000, y: 414}, {x: 1689170400000, y: 415},
                    {x: 1689188400000, y: 416}, {x: 1689282000000, y: 417},
                    {x: 1689336000000, y: 418}, {x: 1689397200000, y: 418},
                    {x: 1689487200000, y: 419}, {x: 1689519600000, y: 419},
                    {x: 1689573600000, y: 419}, {x: 1689588000000, y: 423},
                    {x: 1689595200000, y: 426}, {x: 1689598800000, y: 427},
                    {x: 1689602400000, y: 429}, {x: 1689609600000, y: 430},
                    {x: 1689616800000, y: 431}, {x: 1689624000000, y: 435},
                    {x: 1689627600000, y: 437}, {x: 1689631200000, y: 438},
                    {x: 1689634800000, y: 441}, {x: 1689645600000, y: 441},
                    {x: 1689649200000, y: 442}, {x: 1689656400000, y: 443},
                    {x: 1689660000000, y: 444}, {x: 1689663600000, y: 445},
                    {x: 1689674400000, y: 446}, {x: 1689685200000, y: 446},
                    {x: 1689703200000, y: 447}, {x: 1689706800000, y: 448},
                    {x: 1689728400000, y: 449}, {x: 1689782400000, y: 450},
                    {x: 1689789600000, y: 449}, {x: 1689807600000, y: 450},
                    {x: 1689840000000, y: 450}, {x: 1689854400000, y: 451},
                    {x: 1689861600000, y: 451}, {x: 1689901200000, y: 451},
                    {x: 1689933600000, y: 451}, {x: 1689958800000, y: 452},
                    {x: 1690023600000, y: 451}, {x: 1690038000000, y: 452},
                    {x: 1690102800000, y: 453}, {x: 1690146000000, y: 453},
                    {x: 1690167600000, y: 453}, {x: 1690189200000, y: 454},
                    {x: 1690192800000, y: 456}, {x: 1690203600000, y: 458},
                    {x: 1690243200000, y: 459}, {x: 1690311600000, y: 459},
                    {x: 1690416000000, y: 459},
                ],
                backgroundColor: [
                    'rgba(255, 99, 132, 0.2)',
                ],
                borderColor: [
                    'rgba(255, 99, 132, 1)',
                ],
                borderWidth: 1
            }]
        },
        options: {
          maintainAspectRatio: false,
          plugins: {
            annotation: {
              annotations: {
                first: {
                  type: 'line',
                  borderColor: 'rgba(54, 162, 235, 1)',
                  borderDash: [6, 6],
                  borderWidth: 1,
                  scaleID: 'x',
                  value: 1689091200000
                },
                second: {
                  type: 'line',
                  borderColor: 'rgba(54, 162, 235, 1)',
                  borderDash: [6, 6],
                  borderWidth: 1,
                  scaleID: 'x',
                  value: 1689573600000
                },
                third: {
                  type: 'line',
                  borderColor: 'rgba(54, 162, 235, 1)',
                  borderDash: [6, 6],
                  borderWidth: 1,
                  scaleID: 'x',
                  value: 1690189200000
                }
              }
            }
          },
          scales: {
            x: {
              type: 'time',
              time: {
                unit: 'week',
                displayFormats: {
                    week: 'YYYY-MM-DD'
                },
              tooltipFormat: 'YYYY-MM-DD HH:mm'
              },
              title: {
                display: true,
                text: 'Date'
              }
            },
            y: {
              title: {
                display: true,
                text: 'Members'
              }
            }
          }
        }
    }
    {{< /chart >}}
  {{< /figure >}}
</div>

The chart above provides a detailed view of the number of users joined to the
community Space. It is showcasing every individual join and leave event. To
help in interpreting the data, we added dashed blue lines
indicating the point in time when our bot automatically promoted the space.

Prior to the first dashed line, the joined user count had remained relatively
stagnant for approximately 20 days.

To test, if everything works as expected,
we sent the first promotion message in the middle of the week to the room
and the user count experienced a notable spike. However, as the promotion
message eventually moved out of the view-port, the user count gradually
normalized. This is a common and well-known behavior in many public chats.
The majority of users does not read the room history, when it is outside
the view-port. And for those who do, we made the entire message blue, to
make it stand out.

The second promotion was visible in the view-port for a longer duration and
gave more users time to discover the it. As a result, more users joined
the space, compared to the initial promotion.

Surprisingly, the third message, was pushed out of the view-port right away
which is accurately reflected by the subtle initial response.

Now, let's dive into the developments of the other two rooms.
As you may have guessed, we prepared another chart.
Given the substantial difference in the number of joined users among the three
rooms, we decided to present an unscaled version. To achieve this, we aligned
the starting point of all three rooms to zero at the exact moment of the
first promotion. This approach allows for a direct comparison without the
influence of differing initial user counts and to observe the relative
patterns and trends for all three rooms without any bias.

In reality, at the time of the first promotion, the community space had
402 users, the off-topic room had 234 users, and the meta room had 118 users.

Ideally, if an equal number of users had joined all three rooms following
their respective promotions, we would expect their growth trajectories to be
relatively congruent. However, let's examine the charts to see how they
actually compare.

<div class="text-center"
  {{< figure caption="Members in the Community Space, Off-Topic room and Meta room  (full resolution)" >}}
    {{< chart >}}
    {
        type: 'line',
        data: {
            datasets: [{
                label: 'Community Space',
                lineTension: 0.2,
                pointStyle: false,
                data: [
                     {x: 1689093600000, y: 1}, {x: 1689094800000, y: 2},
                     {x: 1689096000000, y: 6}, {x: 1689097200000, y: 7},
                     {x: 1689100800000, y: 9}, {x: 1689102000000, y: 10},
                     {x: 1689115200000, y: 11}, {x: 1689140400000, y: 12},
                     {x: 1689171600000, y: 13}, {x: 1689188400000, y: 14},
                     {x: 1689194400000, y: 14}, {x: 1689199200000, y: 14},
                     {x: 1689201600000, y: 14}, {x: 1689238800000, y: 14},
                     {x: 1689240000000, y: 14}, {x: 1689283200000, y: 15},
                     {x: 1689284400000, y: 15}, {x: 1689290400000, y: 15},
                     {x: 1689316800000, y: 15}, {x: 1689319200000, y: 15},
                     {x: 1689337200000, y: 16}, {x: 1689338400000, y: 16},
                     {x: 1689397200000, y: 16}, {x: 1689487200000, y: 17},
                     {x: 1689520800000, y: 17}, {x: 1689573600000, y: 17},
                     {x: 1689588000000, y: 19}, {x: 1689589200000, y: 20},
                     {x: 1689590400000, y: 21}, {x: 1689594000000, y: 21},
                     {x: 1689595200000, y: 22}, {x: 1689596400000, y: 23},
                     {x: 1689597600000, y: 24}, {x: 1689598800000, y: 25},
                     {x: 1689602400000, y: 26}, {x: 1689604800000, y: 27},
                     {x: 1689610800000, y: 28}, {x: 1689618000000, y: 29},
                     {x: 1689625200000, y: 33}, {x: 1689627600000, y: 34},
                     {x: 1689628800000, y: 35}, {x: 1689631200000, y: 36},
                     {x: 1689634800000, y: 38}, {x: 1689637200000, y: 39},
                     {x: 1689646800000, y: 39}, {x: 1689650400000, y: 41},
                     {x: 1689651600000, y: 40}, {x: 1689657600000, y: 41},
                     {x: 1689660000000, y: 42}, {x: 1689663600000, y: 43},
                     {x: 1689672000000, y: 43}, {x: 1689675600000, y: 44},
                     {x: 1689685200000, y: 44}, {x: 1689703200000, y: 45},
                     {x: 1689705600000, y: 45}, {x: 1689708000000, y: 46},
                     {x: 1689729600000, y: 47}, {x: 1689735600000, y: 47},
                     {x: 1689784800000, y: 48}, {x: 1689790800000, y: 47},
                     {x: 1689808800000, y: 48}, {x: 1689840000000, y: 48},
                     {x: 1689855600000, y: 49}, {x: 1689864000000, y: 49},
                     {x: 1689902400000, y: 49}, {x: 1689934800000, y: 49},
                     {x: 1689961200000, y: 50}, {x: 1690026000000, y: 49},
                     {x: 1690039200000, y: 50}, {x: 1690105200000, y: 51},
                     {x: 1690148400000, y: 51}, {x: 1690168800000, y: 51},
                     {x: 1690189200000, y: 52}, {x: 1690192800000, y: 54},
                     {x: 1690203600000, y: 55}, {x: 1690204800000, y: 56},
                     {x: 1690209600000, y: 56}, {x: 1690243200000, y: 57},
                     {x: 1690312800000, y: 57},
                ],
                backgroundColor: [
                    'rgba(255, 99, 132, 0.2)',
                ],
                borderColor: [
                    'rgba(255, 99, 132, 1)',
                ],
                borderWidth: 1
            },
            {
                label: 'Off-Topic Room',
                lineTension: 0.2,
                pointStyle: false,
                data: [
                     {x: 1689093600000, y: 2}, {x: 1689094800000, y: 2},
                     {x: 1689096000000, y: 3}, {x: 1689097200000, y: 3},
                     {x: 1689100800000, y: 3}, {x: 1689102000000, y: 3},
                     {x: 1689115200000, y: 3}, {x: 1689140400000, y: 3},
                     {x: 1689171600000, y: 3}, {x: 1689188400000, y: 3},
                     {x: 1689194400000, y: 5}, {x: 1689199200000, y: 4},
                     {x: 1689201600000, y: 5}, {x: 1689238800000, y: 5},
                     {x: 1689240000000, y: 5}, {x: 1689283200000, y: 5},
                     {x: 1689284400000, y: 6}, {x: 1689290400000, y: 5},
                     {x: 1689316800000, y: 6}, {x: 1689319200000, y: 5},
                     {x: 1689337200000, y: 5}, {x: 1689338400000, y: 6},
                     {x: 1689397200000, y: 6}, {x: 1689487200000, y: 7},
                     {x: 1689520800000, y: 7}, {x: 1689573600000, y: 7},
                     {x: 1689588000000, y: 7}, {x: 1689589200000, y: 7},
                     {x: 1689590400000, y: 7}, {x: 1689594000000, y: 8},
                     {x: 1689595200000, y: 8}, {x: 1689596400000, y: 9},
                     {x: 1689597600000, y: 9}, {x: 1689598800000, y: 9},
                     {x: 1689602400000, y: 9}, {x: 1689604800000, y: 10},
                     {x: 1689610800000, y: 11}, {x: 1689618000000, y: 11},
                     {x: 1689625200000, y: 11}, {x: 1689627600000, y: 11},
                     {x: 1689628800000, y: 11}, {x: 1689631200000, y: 11},
                     {x: 1689634800000, y: 12}, {x: 1689637200000, y: 12},
                     {x: 1689646800000, y: 12}, {x: 1689650400000, y: 12},
                     {x: 1689651600000, y: 12}, {x: 1689657600000, y: 12},
                     {x: 1689660000000, y: 12}, {x: 1689663600000, y: 12},
                     {x: 1689672000000, y: 13}, {x: 1689675600000, y: 13},
                     {x: 1689685200000, y: 13}, {x: 1689703200000, y: 14},
                     {x: 1689705600000, y: 14}, {x: 1689708000000, y: 14},
                     {x: 1689729600000, y: 14}, {x: 1689735600000, y: 14},
                     {x: 1689784800000, y: 14}, {x: 1689790800000, y: 13},
                     {x: 1689808800000, y: 13}, {x: 1689840000000, y: 13},
                     {x: 1689855600000, y: 13}, {x: 1689864000000, y: 13},
                     {x: 1689902400000, y: 13}, {x: 1689934800000, y: 13},
                     {x: 1689961200000, y: 13}, {x: 1690026000000, y: 13},
                     {x: 1690039200000, y: 13}, {x: 1690105200000, y: 13},
                     {x: 1690148400000, y: 13}, {x: 1690168800000, y: 13},
                     {x: 1690189200000, y: 13}, {x: 1690192800000, y: 14},
                     {x: 1690203600000, y: 14}, {x: 1690204800000, y: 14},
                     {x: 1690209600000, y: 14}, {x: 1690243200000, y: 14},
                     {x: 1690312800000, y: 14},
                ],
                backgroundColor: [
                    'rgba(153, 102, 255, 0.2)',
                ],
                borderColor: [
                    'rgba(153, 102, 255, 1)',
                ],
                borderWidth: 1
            },
            {
                label: 'Meta Room',
                lineTension: 0.2,
                pointStyle: false,
                data: [
                     {x: 1689093600000, y: 0}, {x: 1689094800000, y: 0},
                     {x: 1689096000000, y: 0}, {x: 1689097200000, y: 0},
                     {x: 1689100800000, y: 0}, {x: 1689102000000, y: 0},
                     {x: 1689115200000, y: 0}, {x: 1689140400000, y: 0},
                     {x: 1689171600000, y: 0}, {x: 1689188400000, y: 0},
                     {x: 1689194400000, y: 0}, {x: 1689199200000, y: 0},
                     {x: 1689201600000, y: 0}, {x: 1689238800000, y: 0},
                     {x: 1689240000000, y: 0}, {x: 1689283200000, y: 0},
                     {x: 1689284400000, y: 1}, {x: 1689290400000, y: 1},
                     {x: 1689316800000, y: 1}, {x: 1689319200000, y: 1},
                     {x: 1689337200000, y: 1}, {x: 1689338400000, y: 2},
                     {x: 1689397200000, y: 2}, {x: 1689487200000, y: 3},
                     {x: 1689520800000, y: 3}, {x: 1689573600000, y: 3},
                     {x: 1689588000000, y: 3}, {x: 1689589200000, y: 3},
                     {x: 1689590400000, y: 3}, {x: 1689594000000, y: 3},
                     {x: 1689595200000, y: 3}, {x: 1689596400000, y: 3},
                     {x: 1689597600000, y: 3}, {x: 1689598800000, y: 3},
                     {x: 1689602400000, y: 3}, {x: 1689604800000, y: 3},
                     {x: 1689610800000, y: 4}, {x: 1689618000000, y: 4},
                     {x: 1689625200000, y: 4}, {x: 1689627600000, y: 4},
                     {x: 1689628800000, y: 4}, {x: 1689631200000, y: 4},
                     {x: 1689634800000, y: 5}, {x: 1689637200000, y: 5},
                     {x: 1689646800000, y: 5}, {x: 1689650400000, y: 5},
                     {x: 1689651600000, y: 5}, {x: 1689657600000, y: 5},
                     {x: 1689660000000, y: 5}, {x: 1689663600000, y: 5},
                     {x: 1689672000000, y: 6}, {x: 1689675600000, y: 6},
                     {x: 1689685200000, y: 6}, {x: 1689703200000, y: 6},
                     {x: 1689705600000, y: 6}, {x: 1689708000000, y: 6},
                     {x: 1689729600000, y: 6}, {x: 1689735600000, y: 5},
                     {x: 1689784800000, y: 5}, {x: 1689790800000, y: 5},
                     {x: 1689808800000, y: 6}, {x: 1689840000000, y: 6},
                     {x: 1689855600000, y: 6}, {x: 1689864000000, y: 6},
                     {x: 1689902400000, y: 6}, {x: 1689934800000, y: 6},
                     {x: 1689961200000, y: 6}, {x: 1690026000000, y: 6},
                     {x: 1690039200000, y: 6}, {x: 1690105200000, y: 6},
                     {x: 1690148400000, y: 6}, {x: 1690168800000, y: 6},
                     {x: 1690189200000, y: 6}, {x: 1690192800000, y: 7},
                     {x: 1690203600000, y: 7}, {x: 1690204800000, y: 7},
                     {x: 1690209600000, y: 8}, {x: 1690243200000, y: 8},
                     {x: 1690312800000, y: 8},
                ],
                backgroundColor: [
                    'rgba(255, 206, 86, 0.2)',
                ],
                borderColor: [
                    'rgba(255, 206, 86, 1)',
                ],
                borderWidth: 1
            }]
        },
        options: {
          maintainAspectRatio: false,
          plugins: {
            annotation: {
              annotations: {
                first: {
                  type: 'line',
                  borderColor: 'rgba(54, 162, 235, 1)',
                  borderDash: [6, 6],
                  borderWidth: 1,
                  scaleID: 'x',
                  value: 1689091200000
                },
                second: {
                  type: 'line',
                  borderColor: 'rgba(54, 162, 235, 1)',
                  borderDash: [6, 6],
                  borderWidth: 1,
                  scaleID: 'x',
                  value: 1689573600000
                },
                third: {
                  type: 'line',
                  borderColor: 'rgba(54, 162, 235, 1)',
                  borderDash: [6, 6],
                  borderWidth: 1,
                  scaleID: 'x',
                  value: 1690189200000
                }
              }
            }
          },
          scales: {
            x: {
              type: 'time',
              time: {
                unit: 'week',
                displayFormats: {
                    week: 'YYYY-MM-DD'
                },
              tooltipFormat: 'YYYY-MM-DD HH:mm'
              },
              title: {
                display: true,
                text: 'Date'
              }
            },
            y: {
              title: {
                display: true,
                text: 'Members'
              }
            }
          }
        }
    }
    {{< /chart >}}
  {{< /figure >}}
</div>

The result is quite disheartening. While it is crucial to acknowledge that
statistical data does not establish a definitive causal relationship,
the observed patterns in the charts suggest there is a correlation between
our promotions and the user joins in the community Space.

For the two rooms, on the other hand, it is hard to argue that there might be
some correlation between the spike in joins in the Space and what happened in
there. Following the first and third promotion, there was no notable surge in
joins, indicating that the joins that happened might be random occurrences
and have nothing to do with the joins in the Space. However, the second
promotion, which remained visible for an extended period in the view-port,
showed a more gradual but still significant initial response
in terms of joins.

Moving forward, the long-term effects of the weekly promotions will
undoubtedly be an area of interest to observe. As we continue these regular
promotions, we aim to assess the cumulative impact and whether sustained
efforts lead to more sustained engagement in the community.

We will definitely continue the experiment. Promoting the Python community
Space is our way of reminding everyone that this place also exists,
in addition to just the Python room. We believe this could over time enhance
the overall user experience. Until we see more users engaging in those other
rooms, we have decided not to add further rooms. Even though some users have
requested more specialized rooms.

So, whether you're a long-time member or a new face in the community, we
invite you to join the Space and engage in vibrant discussions in the
off-topic room!

Thank you for being a part of our community on Matrix,

Happy chatting!

<!-- markdownlint-disable-next-line MD036 -->
