---
title: "Thank You, James!"
description: "Today, James Belchamber left the moderation team"
lead: "Today, James Belchamber left the moderation team."
date: 2023-06-27T01:15:09+02:00
lastmod: 2023-06-27T01:15:09+02:00
draft: false
weight: 50
contributors: ["Michael Sasser"]
categories: ["Community", "Matrix"]
tags: ["Announcement"]
images: ["announcement_python_room.png"]
header_image: ""
floating_image: "announcement_python_room.png"
floating_image_width: "35"
floating_image_position: "right"
floating_image_caption: "Announcement in the Python room"
---

After 881 days or roughly 2 years and 5 months of actively moderating the
Python community on matrix, James Belchamber decided to step down as moderator.

His decision did not come out of the blue. Not too long ago, he informed us,
that he needs more quality time for his family and if you watched his
profile picture over the time, you might have noticed that his family got a
little bigger, twice. Leaving the project and devoting more time to his family
was just the next step. Even though we miss him already, we
completely understand and support his decision.
We are thanking him for the contributions
he made and his efforts to build this community, as well as the time he
dedicated to the project.

In January 2021, James wrote an email to the matrix abuse team to urge
them to clean up the old Python room from spam. Instead, they "tombstoned" the
old room into ours and assigned us the room identifier
{{< identifier "#python:matrix.org" >}}. On the same day, he joined the
moderation team. Since then, he has been a
valued member of the community and the moderation team.

**Thank you, James, for helping us and putting in the time and work for over
two years! We wish you and your family all the best.**

<br />

<!-- markdownlint-disable-next-line MD036 -->
_The Moderation Team_
