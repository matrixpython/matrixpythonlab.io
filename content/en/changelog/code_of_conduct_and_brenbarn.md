---
title: "Code of Conduct & BrenBarn Joined the Moderation Team"
date: 2021-01-04T00:00:00+00:00
lastmod: 2021-01-04T00:00:00+00:00
contributors: ["Michael Sasser"]
draft: false
---

Our Code of Conduct was created. Just a few days later
[BrenBarn]({{< ref "contributors/brenbarn" >}}) joined the moderation team.
