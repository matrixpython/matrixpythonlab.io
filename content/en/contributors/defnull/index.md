---
title: "defnull"
description: "Member of the moderation team"
date: 2022-11-10T17:48:00+01:00
lastmod: 2022-11-10T17:48:00+01:00
draft: false
images: []
matrix_identifier: "@defnull:matrix.cccgoe.de"
matrix_username: "defnull"
matrix_moderator: true
---

If you have any questions, feel free to contact me on Matrix
{{< identifier "@defnull:matrix.cccgoe.de" >}}.
