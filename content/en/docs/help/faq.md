---
title: "FAQ"
description: "Answers to frequently asked questions."
lead: "Answers to frequently asked questions."
date: 2020-10-06T08:49:31+00:00
lastmod: 2020-10-06T08:49:31+00:00
draft: false
images: []
menu:
  docs:
    parent: "help"
weight: 630
toc: false
---

{{< accordion >}}

<!-- How can I report issues? -->
{{< accordion_item name="How can I report issues?" >}}
  If the issue is website related, please follow our guide on how to
  <a href="{{< ref "wiki/Help:Introduction.md#Reporting Issues" >}}">
    report an issue
  </a>
  on GitHub.<br /><br />
  {{< reporting_issues >}}
{{< /accordion_item >}}

<!-- Which language has the Community agreed on? -->
{{< accordion_item name="Which language has the Community agreed on?" >}}
English.
{{< /accordion_item >}}

<!-- Do I need to join the community Space? -->
{{< accordion_item name="Do I need to join the community Space?" >}}
No, this is an optional feature.
{{< /accordion_item >}}

<!-- Are the rooms bridged to ...? -->
{{< accordion_item name="Are the rooms bridged to ...?" >}}
No, and we are not planning to bridge this rooms.<br /> For more information
see
[Why are the rooms not bridged to libera.chat (IRC)? →]({{< ref "#why-are-the-rooms-not-bridged-to-liberachat-irc" >}})
{{< /accordion_item >}}

<!-- Why are the rooms not bridged to libera.chat (IRC)? -->
{{< accordion_item name="Why are the rooms not bridged to libera.chat (IRC)?" >}}
There are multiple reasons, why we are not planning to bridge any of our rooms
to libera.chat.

The appbridge is not and was never stable. There are a bunch of stability and
moderation problems involved.

To join the `#python` IRC channel, you need to be registered on libera.net.
Then you need to join a lobby room first. Otherwise you will be kicked from the
main python channel and invited to the lobby. It is not that big of a
difficulty, when you already know how the IRC usually works. But a new
Matrix user might not. It is not a trivial process, especially
via the appbridge or heisenbridge. There is simply no GUI to do that.
That is a bad user experience we don't want for our community.
It should be easy to join and chat. We are simply not interested in scaring
away users with the complexity involved in bridging to any IRC network.<br />
<br />
For us, the biggest dealbreaker is, that the IRC is not supporting features
and formatting options, Matrix does.
We would have to abandon all the really nice formatting
options and functions matrix has to offer. These include, among others,
Markdown formatting, code blocks, latex math, reactions, message
edits, redactions in general, threaded messages, replies, and media stuff, like
images, videos, and source code files. Otherwise, it
would flood the IRC. Every edit creates a complete new message on IRC.
Messages cannot be redacted. Markdown is displayed as plain-text, every
reaction is shown as its own message, which is hard to bring in context with
the original content (same for threads) and so on.
We cannot simply disable such features on the Matrix end. Every user has to
limit themself solely because we are bridged.<br />
Yes, there are appbridge alternatives, such as heisenbridge, which solves a
few of these problems, but by far not all of them. In addition, it is not tested
for the number of users, we have, let alone the matrix channel on
libera.org<br />
<br />
We like it simple, with all the community building features Matrix has to
offer. We are not interested in limiting us or our users to the standards and
features the IRC has to offer. <br />
Furthermore, we have a [Code of Conduct]({{< ref "code_of_conduct" >}}),
which outlines our expectations for participants within the Matrix community,
from which we do not intend to deviate from.<br />
<br />
If you still want to join `#python` on libera.chat, you can use a dedicated
IRC client or select "Matrix" in "libera.chat" when you click on
"explore rooms" in your matrix client. If you are not on matrix.org, you might
need to add the server yourself.
{{< /accordion_item >}}

<!-- What is a moderator? -->
{{< accordion_item name="What is a moderator?" >}}
A moderator in Matrix is a person given special authority to enforce the rules
in our community. Our moderators are:

{{< moderators >}}
{{< /accordion_item >}}

<!-- How do I become a moderator? -->
{{< accordion_item name="How do I become a moderator?" >}}
In our community it's like the American rock band
[Sugarloaf](<https://en.wikipedia.org/wiki/Sugarloaf_(band)>) sung:

> _Don't call us, We'll call you._

We don't have a form to apply. We keep our eyes and ears open for users, who
are active in the community, willing to help other users, with **useful**
information and know how to be compliant with our [Code of
Conduct]({{< ref "code_of_conduct" >}}).

If we are interested in you to becoming a moderator, we will contact you.
{{< /accordion_item >}}

<!-- How can I contribute to the website? -->
{{< accordion_item name="How can I contribute to the website?" >}}
Follow our guide on how to [contribute]({{< ref "wiki/Help:Introduction" >}}).
{{< /accordion_item >}}

<!-- Q: Who has contributed to the website? -->
{{< accordion_item name="Who has contributed to the website?" >}}
You will find our contributors in the tables below or in the
[about]({{< ref "contributors" >}}) page.
{{< /accordion_item >}}

{{< /accordion >}}
