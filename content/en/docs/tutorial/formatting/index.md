---
title: "Formatting"
description: ""
lead: "Unlocking the Art of Text Transformation: From Plain to Polished!"
date: 2021-02-15T20:05:00+00:00
lastmod: 2021-02-15T20:05:00+00:00
draft: false
images: []
math: true
menu:
  docs:
    parent: "tutorial"
weight: 240
toc: true
---

In the last chapter, you learned how to make a room and how to use it to try
things out.

{{< alert type="primary" >}}
Please do not join a public room and try out the formatting options there.
Create and use a personal test-room instead.
{{< /alert >}}

## Markdown

Markdown is a small markup language for creating formatted text using in
plain-text. Element supports a subset of the Markdown language to help
you formatting your message and thereby create a visually more appealing
content

### Code Blocks

Code blocks are probably not only the most important form of formatting in our
community, we even require them to be used when sharing code.

#### Fenced Code Block

<div><div class="row align-items-center"><div class="col align-items-center">
<p>Fenced Code blocks are primarily used to display code snippets. They ensure
that the code is presented in a fixed-width font and with appropriate
formatting, making it easy to read and distinguish from the regular text.</P>

<p>They preserve the exact spacing and indentation of code, which is crucial
for maintaining code integrity and readability and they prevent Markdown's
default behavior of interpreting multiple spaces as a single space.</p>

<p>The Markdown processor used in element supports syntax highlighting within
fenced code blocks. This feature color-codes different elements of the code
(keywords, comments, strings, etc.) like in a code editor, making it more
understandable and visually appealing</p>

<p>
<!-- prettier-ignore-start -->
To create a fenced code block, begin on an empty line with three backticks
<kbd>`</kbd> like <code>```</code>. To enable syntax highliting, use
three backticks followed by the language name, like <code>```language</code>.
Then, move to the next line and paste your code. To finish the fenced code
block, insert another three backticks <code>```</code> on a separate empty
line.
<!-- prettier-ignore-end -->
<p>Keep in mind that:</p>
<ul>
  <li>not every language is supported by the syntax highlighter</li>
  <li>
      the language name must be precise (see
      <a href="https://github.com/highlightjs/highlight.js/blob/main/SUPPORTED_LANGUAGES.md"
         target="_blank"
         rel="noopener noreferrer"
      >
        highlight.js - Supported Languages
      </a>for more information)
  </li>
  <li>
      if you omit the language after the initial three backticks,
      without any spaces between the language and backticks, it may not
      highlight your code correctly or at all
  </li>
</ul>

<p>Element features automatic language detection for fenced code blocks, if
the language is omitted. It can be enabled in the settings but it often
does not choose the correct language and the syntax highlighting is off</p>

</div><div class="col align-items-center">

<p><h6>Schema</h6></p>

<!-- prettier-ignore-start -->
<div class="highlight"><pre tabindex="0" class="chroma"><button class="btn btn-copy"></button><code class="language-markdown" data-lang="markdown"><span class="line"><span class="cl"></span></span><span class="line"><span class="cl">```language
</span></span><span class="line"><span class="cl">Your (multiline) code here
</span></span><span class="line"><span class="cl">```</span></span></code></pre></div>
<!-- prettier-ignore-end -->

<p class="pt-4"><h6>Example without syntax highlighting</h6></p>

<!-- prettier-ignore-start -->
<div class="highlight"><pre tabindex="0" class="chroma"><button class="btn btn-copy"></button><code class="language-markdown" data-lang="markdown"><span class="line"><span class="cl"></span></span><span class="line"><span class="cl">```
</span></span><span class="line"><span class="cl">def say_hello_to(name: str):
</span></span><span class="line"><span class="cl">    print(f"Hello {name}!")
</span></span><span class="line"><span class="cl">
</span></span><span class="line"><span class="cl">if <span class="gs">__name__</span> == "__main__":
</span></span><span class="line"><span class="cl">    say_hello_to("Michael")
</span></span><span class="line"><span class="cl">```</span></span></code></pre></div>
<!-- prettier-ignore-end -->

<p class="pt-4"><h6>Example with syntax highlighting</h6></p>

<!-- prettier-ignore-start -->
<div class="highlight"><pre tabindex="0" class="chroma"><button class="btn btn-copy"></button><code class="language-markdown" data-lang="markdown"><span class="line"><span class="cl"></span></span><span class="line"><span class="cl">```python
</span></span><span class="line"><span class="cl">def say_hello_to(name: str):
</span></span><span class="line"><span class="cl">    print(f"Hello {name}!")
</span></span><span class="line"><span class="cl">
</span></span><span class="line"><span class="cl">if <span class="gs">__name__</span> == "__main__":
</span></span><span class="line"><span class="cl">    say_hello_to("Michael")
</span></span><span class="line"><span class="cl">```</span></span></code></pre></div>
<!-- prettier-ignore-end -->

</div></div></div>

#### Code Spans

<div><div class="row align-items-center"><div class="col align-items-center">
Code spans or inline code blocks enclose their content within a small box.
This feature isn't limited to programming-related terms or code; it can serve
as a versatile formatting tool for various purposes. It's important to note
that inline code doesn't provide syntax highlighting.

</div><div class="col align-items-center">

```md
`your inline code here` or `here`
```

</div></div></div>

### Emphasis

Text emphasis is a valuable typographic tool that enhances the communication
and readability of written content. It is used to draw attention to specific
words, phrases, or portions of text to convey meaning, emotion, and emphasis
in written communication.

<div><div class="row align-items-center"><div class="col align-items-center">
<br/>
<br/>
<i>Italic</i> text is created by wrapping the text with a single asterisk
<kbd>*</kbd> or underscore <kbd>_</kbd>.
<br/>
<br/>
<b>Bold</b> text is displayed when enclosing content between two
asterisk <kbd>*</kbd> or underscore <kbd>_</kbd> symbols.
<br/>
<br/>
<b><i>Bold Italic</i></b> text can be rendered by using three
asterisk <kbd>*</kbd> or underscore <kbd>_</kbd> symbols.
</div><div class="col align-items-center">

<!-- prettier-ignore-start -->
```md
*This is italic text*
_This is italic text_

**This is bold text**
__This is bold text__

***This is bold italic text***
___This is bold italic text___

```
<!-- prettier-ignore-end -->

</div></div></div>

### Links

<div><div class="row align-items-center"><div class="col align-items-center">

The name of the link is enclosed within square brackets
<kbd>[</kbd> and <kbd>]</kbd> and followed by the URL enclosed within
parentheses <kbd>(</kbd> and <kbd>)</kbd>. The link's name or description
should be something that is easily recognizable for the user, helping them
understand where they will be directed when they click on the link.

</div><div class="col align-items-center">

```md
[Link Name](URL)
```

</div></div></div>

<div><div class="row align-items-center"><div class="col align-items-center">
For example, you wanted to create a link to this website you would do it like
this.
</div><div class="col align-items-center">

```md
[Matrix+Python](https://matrixpython.org)
```

</div></div></div>

### Lists

A list is a structured and itemized presentation of information, typically
consisting of a series of related items, ideas, or tasks.
It serves to simplify content, enhance readability, and facilitate
understanding by organizing data in a clear and concise manner.

Markdown supports two kinds of lists which by themselves or between each type
can be nested.

#### Ordered Lists

<div><div class="row align-items-center"><div class="col align-items-center">
Ordered lists, sometimes also called numbered lists, start with an integer,
followed by either a period <kbd>.</kbd> or alternatively a right parenthesis
<kbd>)</kbd>.

The crucial factor is the integer used for the first item because from there
on, each subsequent integer will be one greater than the preceding one,
even if it's the same number repeated.
This system simplifies the process of adding, removing or re-ordering elements
from the list without the need to manually adjust the numbers each time.

</div><div class="col align-items-center">

<!-- prettier-ignore-start -->
```md
1. First item
2. Second item
3. Third item
44. Fourth item
13. Fifth item
1. Sixth item
```
<!-- prettier-ignore-end -->

</div></div></div>
<div><div class="row align-items-center"><div class="col align-items-center">
Therefore, it's quite common to encounter something like seen in the second
example in Markdown.

In any case, the implementation in element comes with a caveat. As the list
expands and requires double-digit integers, the leftmost elements may not be
displayed correctly.

</div><div class="col align-items-center">

```md
1. First item
1. Second item
1. Third item
1. Fourth item
```

</div></div></div>

#### Unsorted Lists

<div><div class="row align-items-center"><div class="col align-items-center">
Unordered lists start with an asterisk <kbd>*</kbd>, a hyphen character
<kbd>-</kbd> or plus <kbd>+</kbd> symbol.
</div><div class="col align-items-center">

```md
- First item
- Second item
- Third item
- Fourth item
```

</div></div></div>

#### Nested Lists

<div><div class="row align-items-center"><div class="col align-items-center">
We've discussed both ordered and unordered lists, now comes nesting.
Nesting is achieved by adding four spaces for each level of nesting in front
of the element you intend to nest. This means the first level of indentation
consists of four spaces, the second level has eight, and the third level
requires twelve and so on.

</div><div class="col align-items-center">

```md
1. First item
   1. Second item
   2. Third item
      - unordered one
      - unordered two
        - unordered three
2. Fourth item
3. Fifth item
   - another unordered element
4. Sixth item
```

</div></div></div>

### Headings

<div><div class="row align-items-center"><div class="col align-items-center">
Headings produce large to massive sized fonts. They are created using the pound
(or hash/hashtag) character <kbd>#</kbd> at the beginning of a line followed
by text. The number of pound symbols used determines the size of the heading.
One pound character infornt of text symbolizes the largest available heading.
Five of them the smallest one.

In most messages they are more disrupting than usful, so be considerate with
using them. For structuring large text H5 (five pound symbols) to H4
(four pound symbols) are usually enough.

</div><div class="col align-items-center">

```md
# Heading H1

## Heading H2

### Heading H3

#### Heading H4

##### Heading H5
```

</div></div></div>

### Blockquote

<div><div class="row align-items-center"><div class="col align-items-center">
Blockquotes are typically employed for citations, introducing text with a
line and slightly muted font. To create a blockquote, begin with a
greater-than sign <kbd>></kbd> followed by a space and the text.
You can create nested blockquotes with multiple levels by using one
greater-than sign, separated by one space for each level.
Blockquotes are not limited to text; they can encompass various Markdown
formatting supported.

</div><div class="col align-items-center">

```md
> This could be some citation using `blockquotes`.
> Another
>
> > Even nested blockquotes
> > are possible!
```

</div></div></div>

### Horizontal Rule

<div><div class="row align-items-center"><div class="col align-items-center">
Three dashes <kbd>-</kbd> like <code>---</code> at the beginning of a line
are rendered as a horizontal line that extends over the entire message body.

Please take note of the empty line between the three horizontal
dashes and the two sentences in the example.
It's presence is crucial to ensure consistent rendering; without it, the
results may vary. However, at the start or end of a message in the message
bodies boundaries, this empty line is not required.

In Element, these horizontal dashes are not rendered subtly. They appear
rather bold and can be quite disruptive.

</div><div class="col align-items-center">

```md
Some example text

---

Another example text
```

</div></div></div>

## LaTeX Math

Unfortunately, not all Matrix clients offer support for $ {\\LaTeX} $ math yet.
In fact, many clients, including Element, don't even have this feature
enabled by default. To enable it in Element, you'll need a client that has
the "labs" feature activated, in which you can enable the feature
"Render LaTeX maths in messages".

Delving into an extensive discussion on LaTeX math is beyond the scope of this
tutorial. Thus, we'll focus solely on the specifics related to element.

{{< alert preset="todo" >}}{{< /alert >}}

<!-- - inline: `$a + b = 7$` -->
<!-- - block: `$$a + b = 7$$` -->
