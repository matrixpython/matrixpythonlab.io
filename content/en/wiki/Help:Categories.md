---
title: "Help:Categories"
url: "/wiki/Help\\:Categories"
description: ""
lead: "Create a blog category."
date: 2022-07-17T06:19:08+02:00
lastmod: 2022-07-17T06:19:08+02:00
contributors: ["Michael Sasser"]
wiki_categories: ["help", "contribute", "layout"]
draft: false
images: []
weight: 50
toc: true
---

Blog categories are a way to group blog posts in a way, users are able to
filter for. When a category was used that does not exist, a dummy category is
shown on the categories page. Because of that, make sure to create a category
for your blog post, if it doesn't already exist.

## Location

| Directory                                                        | Description                               |
| ---------------------------------------------------------------- | ----------------------------------------- |
| [content/\<Language\>/categories/]({{< reporef "categories" >}}) | The base directory of the blog categories |

{{% newpage name="categories" %}}

## Front Matter

{{< variable_structure "Categories" "title" "description" "lead" "date" "lastmod" "contributors" "draft" "images" "math" >}}

## Code

Below you find the implementation of the layout.

### HTML

#### Term Page Layout

Defined in `layouts/categories/term.html`.

{{< alert preset="license-code" >}}{{< /alert >}}

{{% readfile path="/layouts/categories/term.html" %}}

#### Terms Page Layout

Defined in `layouts/categories/terms.html`.

{{< alert preset="license-code" >}}{{< /alert >}}

{{% readfile path="/layouts/categories/terms.html" %}}

### Archetype

Defined in `layouts/categories.md`.

{{< alert preset="license-code" >}}{{< /alert >}}

{{% readfile path="/archetypes/categories.md" type="yaml" %}}
