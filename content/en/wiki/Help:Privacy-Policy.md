---
title: "Help:Privacy-Policy"
url: "/wiki/Help\\:Privacy-Police"
description: ""
lead: "Our privacy GDPR and imprint."
date: 2022-07-17T06:19:08+02:00
lastmod: 2022-07-17T06:19:08+02:00
contributors: ["Michael Sasser"]
wiki_categories: ["help", "contribute", "layout"]
draft: false
images: []
weight: 50
toc: true
---

This directory contains out privacy-policy (GDPR; the
[General Data Protection Regulation](https://en.wikipedia.org/wiki/General_Data_Protection_Regulation)
([(EU) 2016/679](https://eur-lex.europa.eu/eli/reg/2016/679/oj)) is a
regulation in European law on data protection and privacy in the EU.) and our
imprint.

{{< alert >}}
Do not use any images in the privacy-policy or imprint.
{{< /alert >}}

## Location

| Directory                                                                                | Description                                |
| ---------------------------------------------------------------------------------------- | ------------------------------------------ |
| [content/\<Language\>/privacy-policy/]({{< reporef "privacy-policy" >}})                 | The base directory of the GDPR and imprint |
| [content/\<Language\>/privacy-policy/gdpr/]({{< reporef "privacy-policy/imprint" >}})    | Contains the imprint                       |
| [content/\<Language\>/privacy-policy/imprint/]({{< reporef "privacy-policy/imprint" >}}) | Contains the imprint                       |

{{% newpage name="privacy-policy" %}}

## Front Matter

{{< variable_structure "Privacy-Policy" "title" "description" "date" "lastmod" "contributors" "draft" "weight" "images" "math" "menu" "toc" >}}

## Code

The privacy-policy uses the default layout.

{{< alert preset="todo" >}}{{< /alert >}}

{{< ln "docs" >}}
