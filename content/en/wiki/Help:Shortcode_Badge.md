---
title: "Help:Shortcode Badge"
url: "/wiki/Help\\:Shortcode_Badge"
description: ""
lead: "Create a badge."
date: 2022-11-17T15:43:00+01:00
lastmod: 2022-11-17T15:43:00+01:00
contributors: ["Michael Sasser"]
wiki_categories: ["help", "contribute", "shortcode"]
draft: false
images: []
weight: 50
toc: true

infobox:
  header: Badge
  data:
    - type: subheader
      header: '[Shortcode]({{< ref "/wiki_categories/shortcode" >}})'
    - type: header
      header: Description
    - type: label
      label: Type
      value: HTML Shortcode
    - type: label
      label: Nested?
      value: No
    - type: label
      label: Shortcode
      value: "`Badge`"
    - type: label
      label: Return Type
      value: HTML
    - type: fulldata
      label: Short Description
      value: |
        Display a Badge
    - type: header
      header: Development
    - type: label
      label: Maintainer
      value: '[Michael Sasser]({{< ref "michael-sasser" >}})'
---

Use the shortcode `Badge` to display a square, rounded or pill shaped box
with a small title in it.

## Parameters

The Badge shortcode has the following parameters:

| Parameter | Default   | Description                                                                                                            |
| --------- | --------- | ---------------------------------------------------------------------------------------------------------------------- |
| `title`   | -         | The badge title or text.                                                                                               |
| `shape`   | `rounded` | The shape of the badge. It can be `rounded`, `square` or `pill` shaped.                                                |
| `type`    | `primary` | The type of the badge, which sets it's color. The type can be `primary`, `secondary` `success`, `warning` or `danger`. |

## Overview

The following matrix shows all possible options for the badge shortcode.

|               | rounded                                                                   | square                                                                  | pill                                                                |
| ------------- | ------------------------------------------------------------------------- | ----------------------------------------------------------------------- | ------------------------------------------------------------------- |
| **primary**   | {{< badge title="primary, rounded" shape="rounded" type="primary" >}}     | {{< badge title="primary, square" shape="square" type="primary" >}}     | {{< badge title="primary, pill" shape="pill" type="primary" >}}     |
| **secondary** | {{< badge title="secondary, rounded" shape="rounded" type="secondary" >}} | {{< badge title="secondary, square" shape="square" type="secondary" >}} | {{< badge title="secondary, pill" shape="pill" type="secondary" >}} |
| **success**   | {{< badge title="success, rounded" shape="rounded" type="success" >}}     | {{< badge title="success, square" shape="square" type="success" >}}     | {{< badge title="success, pill" shape="pill" type="success" >}}     |
| **warning**   | {{< badge title="warning, rounded" shape="rounded" type="warning" >}}     | {{< badge title="warning, square" shape="square" type="warning" >}}     | {{< badge title="warning, pill" shape="pill" type="warning" >}}     |
| **danger**    | {{< badge title="danger, rounded" shape="rounded" type="danger" >}}       | {{< badge title="danger, square" shape="square" type="danger" >}}       | {{< badge title="danger, pill" shape="pill" type="danger" >}}       |

## Examples

```md
{{</* badge title="foo" shape="rounded" type="primary" */>}}
{{</* badge title="bar" */>}}
```

<!-- prettier-ignore-start -->
{{< rendered >}}
{{< badge title="foo" shape="rounded" type="primary" >}}
{{< badge title="bar" >}}
{{< /rendered >}}
<!-- prettier-ignore-end -->

## Code

Below you find the implementation of the shortcode.

### HTML

Defined in `layouts/shortcodes/badge.html`.

{{< alert preset="license-code" >}}{{< /alert >}}

{{% readfile path="/layouts/shortcodes/badge.html" %}}

#### Download

Copy the source code above or use the download link below to use this file on
your website according to the license.

{{< download title="Badge Shortcode" path="/layouts/shortcodes/badge.html" >}}

### SASS

Defined in: `/assets/scss/components/_shortcode_badge.scss`

{{< alert preset="license-code" >}}{{< /alert >}}

{{% readfile path="/assets/scss/components/_shortcode_badge.scss" %}}

#### Download

Copy the source code above or use the download link below to use this file on
your website according to the license.

{{< download title="Badge Style" path="/assets/scss/components/_shortcode_badge.scss" >}}
