---
title: "Help:Shortcode Chart"
url: "/wiki/Help\\:Shortcode_Chart"
description: ""
lead: "Create a simple chart."
date: 2023-07-27T10:25:00+01:00
lastmod: 2023-07-27T10:25:00+01:00
contributors: ["Michael Sasser"]
wiki_categories: ["help", "contribute", "shortcode"]
draft: false
images: []
weight: 50
toc: true

infobox:
  header: Chart
  data:
    - type: subheader
      header: '[Shortcode]({{< ref "/wiki_categories/shortcode" >}})'
    - type: header
      header: Description
    - type: label
      label: Type
      value: HTML Shortcode
    - type: label
      label: Nested?
      value: No
    - type: label
      label: Shortcode
      value: "`chart`"
    - type: label
      label: Return Type
      value: HTML with inline JS
    - type: fulldata
      label: Short Description
      value: |
        Render a charts using <code>chart.js</code>.
    - type: header
      header: Extensions Available
    - type: label
      label: Plugins
      value: "[annotation](https://www.chartjs.org/chartjs-plugin-annotation/latest/)"
    - type: label
      label: Adapters
      value: "[moment](https://github.com/chartjs/chartjs-adapter-moment)"
    - type: header
      header: Development
    - type: label
      label: Maintainer
      value: '[Michael Sasser]({{< ref "michael-sasser" >}})'
---

Use the `chart` shortcode to render charts using
[chart.js](https://www.chartjs.org/).

## Parameters

The Chart shortcode has the following parameters:

| Parameter | Description                                                              |
| --------- | ------------------------------------------------------------------------ |
| `width`   | The width of the chart in percent of the available space. Default: `100` |
| `height`  | The height of the chart in `px`. Default: `400`                          |
| `[0]`     | The `chart.js` options as `JS`                                           |

## Examples

<!-- prettier-ignore-start -->
```js
{{</* chart height=250 width=50 */>}}
{
    type: 'doughnut',
    data: {
        labels: ['Python', 'Community', 'Off-Topic', 'Meta', 'Space'],
        datasets: [{
            data: [13179, 484, 262, 142, 23],
            backgroundColor: [
                'rgba(255, 99, 132, 0.2)',
                'rgba(54, 162, 235, 0.2)',
                'rgba(255, 206, 86, 0.2)',
                'rgba(75, 192, 192, 0.2)',
                'rgba(153, 102, 255, 0.2)',
            ],
            borderColor: [
                'rgba(255, 99, 132, 1)',
                'rgba(54, 162, 235, 1)',
                'rgba(255, 206, 86, 1)',
                'rgba(75, 192, 192, 1)',
                'rgba(153, 102, 255, 1)',
            ],
        }]
    },
    options: {
        responsive: true,
        maintainAspectRatio: false,
        plugins: {legend: {position: 'right'}}
    }
}
{{</*/ chart */>}}
```
<!-- prettier-ignore-end -->

<!-- prettier-ignore-start -->
```js
{{</* chart */>}}
{
    type: 'line',
    data: {
        datasets: [{
            label: 'Joined Members',
            lineTension: 0.2,
            pointStyle: false,
            data: [
                {x: 1688479200000, y: 398},
                {x: 1688508000000, y: 399},
                {x: 1688529600000, y: 400},
                {x: 1688680800000, y: 399},
                {x: 1688691600000, y: 400},
                {x: 1688713200000, y: 401},
                {x: 1688734800000, y: 402},
                {x: 1688738400000, y: 401},
                {x: 1688742000000, y: 402},
                {x: 1688821200000, y: 401},
                {x: 1688850000000, y: 400},
                {x: 1688929200000, y: 400},
                {x: 1688986800000, y: 399},
                {x: 1689091200000, y: 403},
                {x: 1689094800000, y: 409},
                {x: 1689098400000, y: 411},
                {x: 1689102000000, y: 412},
                {x: 1689112800000, y: 413},
                {x: 1689138000000, y: 414},
                {x: 1689170400000, y: 415},
                {x: 1689188400000, y: 416},
                {x: 1689282000000, y: 417},
                {x: 1689336000000, y: 418},
                {x: 1689397200000, y: 418},
                {x: 1689487200000, y: 419},
                {x: 1689519600000, y: 419},
                {x: 1689573600000, y: 419},
                {x: 1689588000000, y: 423},
                {x: 1689595200000, y: 426},
                {x: 1689598800000, y: 427},
                {x: 1689602400000, y: 429},
                {x: 1689609600000, y: 430},
                {x: 1689616800000, y: 431},
                {x: 1689624000000, y: 435},
                {x: 1689627600000, y: 437},
                {x: 1689631200000, y: 438},
                {x: 1689634800000, y: 441},
                {x: 1689645600000, y: 441},
                {x: 1689649200000, y: 442},
                {x: 1689656400000, y: 443},
                {x: 1689660000000, y: 444},
                {x: 1689663600000, y: 445},
                {x: 1689674400000, y: 446},
                {x: 1689685200000, y: 446},
                {x: 1689703200000, y: 447},
                {x: 1689706800000, y: 448},
                {x: 1689728400000, y: 449},
                {x: 1689782400000, y: 450},
                {x: 1689789600000, y: 449},
                {x: 1689807600000, y: 450},
                {x: 1689840000000, y: 450},
                {x: 1689854400000, y: 451},
                {x: 1689861600000, y: 451},
                {x: 1689901200000, y: 451},
                {x: 1689933600000, y: 451},
                {x: 1689958800000, y: 452},
                {x: 1690023600000, y: 451},
                {x: 1690038000000, y: 452},
                {x: 1690102800000, y: 453},
                {x: 1690146000000, y: 453},
                {x: 1690167600000, y: 453},
                {x: 1690189200000, y: 454},
                {x: 1690192800000, y: 456},
                {x: 1690203600000, y: 458},
                {x: 1690243200000, y: 459},
                {x: 1690311600000, y: 459},
                {x: 1690416000000, y: 459},
            ],
            backgroundColor: [
                'rgba(255, 99, 132, 0.2)',
            ],
            borderColor: [
                'rgba(255, 99, 132, 1)',
            ],
            borderWidth: 1
        }]
    },
    options: {
      maintainAspectRatio: false,
      plugins: {
        annotation: {
          annotations: {
            first: {
              type: 'line',
              borderColor: 'rgba(54, 162, 235, 1)',
              borderDash: [6, 6],
              borderWidth: 1,
              scaleID: 'x',
              value: 1689091200000
            },
            second: {
              type: 'line',
              borderColor: 'rgba(54, 162, 235, 1)',
              borderDash: [6, 6],
              borderWidth: 1,
              scaleID: 'x',
              value: 1689573600000
            },
            third: {
              type: 'line',
              borderColor: 'rgba(54, 162, 235, 1)',
              borderDash: [6, 6],
              borderWidth: 1,
              scaleID: 'x',
              value: 1690189200000
            }
          }
        },
        title: {
          display: true,
          text: 'Room Membership in the Python Community Space'
        },
      },
      scales: {
        x: {
          type: 'time',
          time: {
            unit: 'week',
            displayFormats: {
                week: 'YYYY-MM-DD'
            },
            tooltipFormat: 'YYYY-MM-DD'
          },
          title: {
            display: true,
            text: 'Date'
          }
        },
        y: {
          title: {
            display: true,
            text: 'Members'
          }
        }
      }
    }
}
{{</*/ chart */>}}
```
<!-- prettier-ignore-end -->

<!-- prettier-ignore-start -->
{{< rendered >}}
{{< chart height=250 width=50 >}}
{
    type: 'doughnut',
    data: {
        labels: ['Python', 'Community', 'Off-Topic', 'Meta', 'Space'],
        datasets: [{
            data: [13179, 484, 262, 142, 23],
            backgroundColor: [
                'rgba(255, 99, 132, 0.2)',
                'rgba(54, 162, 235, 0.2)',
                'rgba(255, 206, 86, 0.2)',
                'rgba(75, 192, 192, 0.2)',
                'rgba(153, 102, 255, 0.2)',
            ],
            borderColor: [
                'rgba(255, 99, 132, 1)',
                'rgba(54, 162, 235, 1)',
                'rgba(255, 206, 86, 1)',
                'rgba(75, 192, 192, 1)',
                'rgba(153, 102, 255, 1)',
            ],
        }]
    },
    options: {
        responsive: true,
        maintainAspectRatio: false,
        plugins: {legend: {position: 'right'}}
    }
}
{{< /chart >}}
{{</ rendered >}}
<!-- prettier-ignore-end -->

<!-- prettier-ignore-start -->
{{< rendered >}}
{{< chart >}}
{
    type: 'line',
    data: {
        datasets: [{
            label: 'Joined Members',
            lineTension: 0.2,
            pointStyle: false,
            data: [
                {x: 1688479200000, y: 398},
                {x: 1688508000000, y: 399},
                {x: 1688529600000, y: 400},
                {x: 1688680800000, y: 399},
                {x: 1688691600000, y: 400},
                {x: 1688713200000, y: 401},
                {x: 1688734800000, y: 402},
                {x: 1688738400000, y: 401},
                {x: 1688742000000, y: 402},
                {x: 1688821200000, y: 401},
                {x: 1688850000000, y: 400},
                {x: 1688929200000, y: 400},
                {x: 1688986800000, y: 399},
                {x: 1689091200000, y: 403},
                {x: 1689094800000, y: 409},
                {x: 1689098400000, y: 411},
                {x: 1689102000000, y: 412},
                {x: 1689112800000, y: 413},
                {x: 1689138000000, y: 414},
                {x: 1689170400000, y: 415},
                {x: 1689188400000, y: 416},
                {x: 1689282000000, y: 417},
                {x: 1689336000000, y: 418},
                {x: 1689397200000, y: 418},
                {x: 1689487200000, y: 419},
                {x: 1689519600000, y: 419},
                {x: 1689573600000, y: 419},
                {x: 1689588000000, y: 423},
                {x: 1689595200000, y: 426},
                {x: 1689598800000, y: 427},
                {x: 1689602400000, y: 429},
                {x: 1689609600000, y: 430},
                {x: 1689616800000, y: 431},
                {x: 1689624000000, y: 435},
                {x: 1689627600000, y: 437},
                {x: 1689631200000, y: 438},
                {x: 1689634800000, y: 441},
                {x: 1689645600000, y: 441},
                {x: 1689649200000, y: 442},
                {x: 1689656400000, y: 443},
                {x: 1689660000000, y: 444},
                {x: 1689663600000, y: 445},
                {x: 1689674400000, y: 446},
                {x: 1689685200000, y: 446},
                {x: 1689703200000, y: 447},
                {x: 1689706800000, y: 448},
                {x: 1689728400000, y: 449},
                {x: 1689782400000, y: 450},
                {x: 1689789600000, y: 449},
                {x: 1689807600000, y: 450},
                {x: 1689840000000, y: 450},
                {x: 1689854400000, y: 451},
                {x: 1689861600000, y: 451},
                {x: 1689901200000, y: 451},
                {x: 1689933600000, y: 451},
                {x: 1689958800000, y: 452},
                {x: 1690023600000, y: 451},
                {x: 1690038000000, y: 452},
                {x: 1690102800000, y: 453},
                {x: 1690146000000, y: 453},
                {x: 1690167600000, y: 453},
                {x: 1690189200000, y: 454},
                {x: 1690192800000, y: 456},
                {x: 1690203600000, y: 458},
                {x: 1690243200000, y: 459},
                {x: 1690311600000, y: 459},
                {x: 1690416000000, y: 459},
            ],
            backgroundColor: [
                'rgba(255, 99, 132, 0.2)',
            ],
            borderColor: [
                'rgba(255, 99, 132, 1)',
            ],
            borderWidth: 1
        }]
    },
    options: {
      maintainAspectRatio: false,
      plugins: {
        annotation: {
          annotations: {
            first: {
              type: 'line',
              borderColor: 'rgba(54, 162, 235, 1)',
              borderDash: [6, 6],
              borderWidth: 1,
              scaleID: 'x',
              value: 1689091200000
            },
            second: {
              type: 'line',
              borderColor: 'rgba(54, 162, 235, 1)',
              borderDash: [6, 6],
              borderWidth: 1,
              scaleID: 'x',
              value: 1689573600000
            },
            third: {
              type: 'line',
              borderColor: 'rgba(54, 162, 235, 1)',
              borderDash: [6, 6],
              borderWidth: 1,
              scaleID: 'x',
              value: 1690189200000
            }
          }
        },
        title: {
          display: true,
          text: 'Room Membership in the Python Community Space'
        },
      },
      scales: {
        x: {
          type: 'time',
          time: {
            unit: 'week',
            displayFormats: {
                week: 'YYYY-MM-DD'
            },
            tooltipFormat: 'YYYY-MM-DD'
          },
          title: {
            display: true,
            text: 'Date'
          }
        },
        y: {
          title: {
            display: true,
            text: 'Members'
          }
        }
      }
    }
}
{{< /chart >}}
{{< /rendered >}}
<!-- prettier-ignore-end -->

## Code

Below you find the implementation of the shortcode.

### HTML

Defined in `layouts/shortcodes/chart.html`.

{{< alert preset="license-code" >}}{{< /alert >}}

{{% readfile path="/layouts/shortcodes/chart.html" %}}

#### Download

Copy the source code above or use the download link below to use this file on
your website according to the license.

{{< download title="Ln Shortcode" path="/layouts/shortcodes/chart.html" >}}
