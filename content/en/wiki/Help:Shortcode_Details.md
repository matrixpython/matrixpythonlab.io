---
title: "Help:Shortcode Details"
url: "/wiki/Help\\:Shortcode_Details"
description: ""
lead: "Display something inside an expandable HTML container."
date: 2022-07-17T06:19:08+02:00
lastmod: 2022-07-17T06:19:08+02:00
contributors: ["Michael Sasser"]
wiki_categories: ["help", "contribute", "shortcode"]
draft: false
images: []
weight: 50
toc: true

infobox:
  header: Details
  data:
    - type: subheader
      header: '[Shortcode]({{< ref "/wiki_categories/shortcode" >}})'
    - type: header
      header: Description
    - type: label
      label: Type
      value: HTML Shortcode
    - type: label
      label: Nested?
      value: Yes
    - type: label
      label: Shortcode
      value: "`details`"
    - type: label
      label: Return Type
      value: HTML
    - type: fulldata
      label: Short Description
      value: |
        Hide information in an vertically expandable container.
    - type: header
      header: Development
    - type: label
      label: Maintainer
      value: '[Michael Sasser]({{< ref "michael-sasser" >}})'
---

When only certain information is needed, some vertically extensible
information can be
stored in an expandable container, so the viewer does not get overwhelmed by
too much information. A user can, if they are interested in the information,
extend or collapse it, at any given moment.

## Parameters

The details shortcode has the following parameters:

| Parameter  | Description              |
| ---------- | ------------------------ |
| `[0]`      | The title of the details |
| `[.Inner]` | The content              |

## Examples

```md
{{</* details "Hello" */>}}
Hello Details
{{</* /details */>}}
```

<!-- prettier-ignore-start -->
{{< rendered >}}
{{< details "Hello" >}}
Hello World
{{< /details >}}
{{< /rendered >}}
<!-- prettier-ignore-end -->

## Code

Below you find the implementation of the shortcode.

### HTML

Defined in `layouts/shortcodes/details.html`.

{{< alert preset="license-code" >}}{{< /alert >}}

{{% readfile path="/layouts/shortcodes/details.html" %}}

#### Download

Copy the source code above or use the download link below to use this file on
your website according to the license.

{{< download title="Details Shortcode" path="/layouts/shortcodes/details.html" >}}

### SASS

Defined in: `/assets/scss/components/_shortcode_details.scss`

{{< alert preset="license-code" >}}{{< /alert >}}

{{% readfile path="/assets/scss/components/_shortcode_details.scss" %}}

#### Download

Copy the source code above or use the download link below to use this file on
your website according to the license.

{{< download title="Details Style" path="/assets/scss/components/_shortcode_details.scss" >}}
