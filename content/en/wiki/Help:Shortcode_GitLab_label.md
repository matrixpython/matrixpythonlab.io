---
title: "Help:Shortcode GitLab Label"
url: "/wiki/Help\\:Shortcode_GitLab_Label"
description: ""
lead: "Use GitLab Labels as badges in a page."
date: 2022-07-17T06:19:08+02:00
lastmod: 2022-07-17T06:19:08+02:00
contributors: ["Michael Sasser"]
wiki_categories: ["help", "contribute", "shortcode"]
draft: false
images: []
weight: 50
toc: true

infobox:
  header: GitLab Label
  data:
    - type: subheader
      header: '[Shortcode]({{< ref "/wiki_categories/shortcode" >}})'
    - type: header
      header: Description
    - type: label
      label: Type
      value: HTML Shortcode
    - type: label
      label: Nested?
      value: No
    - type: label
      label: Shortcode
      value: "`gitlab_label`"
    - type: label
      label: Return Type
      value: HTML
    - type: fulldata
      label: Short Description
      value: |
        Display value or key-value-pair badge, which look like a GitLab lable.
    - type: header
      header: Development
    - type: label
      label: Maintainer
      value: '[Michael Sasser]({{< ref "michael-sasser" >}})'
---

With the `gitlab_label` shortcode you can create labels which can
be linked to the GitLab issue list. They look exactly like the labels used in
GitLab.

Besides some presets, labels are not hard-coded to this project, our group or
any subgroup on Gitlab. This makes them more flexible. On the other hand, they
need more information when they are linked.

For example
[our group](https://gitlab.com/matrixpython) has the URL
`https://gitlab.com/matrixpython`. The label needs an `id` parameter which is
`matrixpython`.

[This website](https://gitlab.com/matrixpython/website) has
the URL `https://gitlab.com/matrixpython/website`. The label
needs an `id` label, which is `matrixpython/website`.

To make it easier for our common triage process labels, we check the name of
the label (`label`) against a hashtable. If the label is in found,
the `color` of the label and the `id` is automatically set/overwritten and
linked to the issue list. The hashtable is hard-coded in the shortcodes code.

Our triage process labels can be found in the eponymous documentation.

{{< ln "/wiki/Help:Triage_Process" >}}

## Parameters

The GitLab Label shortcode has the following parameters:

| Parameter | Description                                                                                                                             |
| --------- | --------------------------------------------------------------------------------------------------------------------------------------- |
| `label`   | The label syntax used on GitLab                                                                                                         |
| `color`   | Set a color to the label                                                                                                                |
| `id`      | The part of the URL with is used to identify the project, group or subgroup. For example `matrixpython/website` for the website project |
| `inline`  | When `true` the label is rendered smaller for inline use                                                                                |

## Examples

This example creates three arbitrary labels.

```md
{{</* gitlab_label label="labelName" color="#3399cc" */>}}

{{</* gitlab_label label="another::scope" color="#aa3300" */>}}

<!-- The default color is #aa0000 -->

{{</* gitlab_label label="defaultColor" */>}}
```

<!-- prettier-ignore-start -->
{{< rendered >}}
{{< gitlab_label label="labelName" color="#3399cc" >}}

{{< gitlab_label label="another::scope" color="#aa3300" >}}

{{< gitlab_label label="defaultColor" >}}
{{< /rendered >}}
<!-- prettier-ignore-end -->

The following example uses a label which is a, common in our triage process.
Additional information, such as the color, id and whether they are linked to
the issue list are overwritten by a hashmap in the shortcodes code.

The last example shows how labels can be used inline.

```md
{{</* gitlab_label label="Exception::Release Blocker" */>}}

{{</* gitlab_label label="Exception::Release Blocker" color="#aa3300" */>}}

{{</* gitlab_label label="Exception::Release Blocker" color="#3399cc" id="foo" */>}}

<br /><br />
An inline {{</* gitlab_label label="Exception::Release Blocker" inline=true */>}} example.
```

<!-- prettier-ignore-start -->
{{< rendered >}}
{{< gitlab_label label="Exception::Release Blocker" >}}

{{< gitlab_label label="Exception::Release Blocker" color="#aa3300" >}}

{{< gitlab_label label="Exception::Release Blocker" color="#3399cc" id="foo" >}}

<br /><br />
An inline {{< gitlab_label label="Exception::Release Blocker" inline=true >}} example.
{{< /rendered >}}
<!-- prettier-ignore-end -->

Maintainer labels will only be linked, when a `id` is given, except for group
labels. They default to the id `matrixpython`. The color is automatically
overwritten.

```md
<!-- A maintainer group label, the id defaults to our group -->

{{</* gitlab_label label="G-Does Not Exist" */>}}

<!-- A maintainer subgroup label, no id is given, not linked -->

{{</* gitlab_label label="S-Does Not Exist Either" */>}}

<!-- A maintainer project label, where the id is given, so it will be linked -->

{{</* gitlab_label label="P-foo" id="matrixpython/website" */>}}
```

<!-- prettier-ignore-start -->
{{< rendered >}}
{{< gitlab_label label="G-Does Not Exist" >}}

{{< gitlab_label label="S-Does Not Exist Either" >}}

{{< gitlab_label label="P-foo" id="matrixpython/website" >}}
{{< /rendered >}}
<!-- prettier-ignore-end -->

## Code

Below you find the implementation of the shortcode.

### HTML

Defined in `layouts/shortcodes/gitlab_label.html`.

{{< alert preset="license-code" >}}{{< /alert >}}

{{% readfile path="/layouts/shortcodes/gitlab_label.html" %}}

#### Download

Copy the source code above or use the download link below to use this file on
your website according to the license.

{{< download title="GitLab Label Shortcode" path="/layouts/shortcodes/gitlab_label.html" >}}

### SASS

Defined in: `/assets/scss/components/_shortcode_gitlab_label.scss`

{{< alert preset="license-code" >}}{{< /alert >}}

{{% readfile path="/assets/scss/components/_shortcode_gitlab_label.scss" %}}

#### Download

Copy the source code above or use the download link below to use this file on
your website according to the license.

{{< download title="Gitlab Label Style" path="/assets/scss/components/_shortcode_gitlab_label.scss" >}}
