---
title: "Help:Shortcode Identifier"
url: "/wiki/Help\\:Shortcode_Identifier"
description: ""
lead: "Display Matrix identifiers and email addresses."
date: 2022-07-17T06:19:08+02:00
lastmod: 2022-07-17T06:19:08+02:00
contributors: ["Michael Sasser"]
wiki_categories: ["help", "contribute", "shortcode"]
draft: false
images: []
weight: 50
toc: true

infobox:
  header: Identifier
  data:
    - type: subheader
      header: '[Shortcode]({{< ref "/wiki_categories/shortcode" >}})'
    - type: header
      header: Description
    - type: label
      label: Type
      value: HTML Shortcode
    - type: label
      label: Nested?
      value: No
    - type: label
      label: Shortcode
      value: "`identifier`"
    - type: label
      label: Return Type
      value: HTML
    - type: fulldata
      label: Short Description
      value: |
        Display pill shaped with the user/room/email identifier badges with
        an round avatar/logo in the front.
    - type: header
      header: Development
    - type: label
      label: Maintainer
      value: '[Michael Sasser]({{< ref "michael-sasser" >}})'
---

The `identifier` shortcode renders an representation of an matrix
identifier (user or room) or email-address as an pill shaped badge with an
avatar or logo. The shortcode is able
to differentiate between the different identifiers and renders them
accordingly.

Matrix user and room identifiers are created with the user avatar or room
logo as tiny images in the front of the pill shaped badge.
Those images are coming from `matrix.org` directly. Only metadata, such as
the username identifiers or room identifiers together with their avatar/logo
URI are stored as `json` in an CI step during an production build of the
website. When an user changed their avatar, the old avatar is still shown until
the page gets updated or the user deletes the old avatar from matrix.

During development, the metadata stored in those `json` files are not available
automatically. They are rendered just like the user or room has no avatar/logo.
If you need to render the images during development, you need to download them
yourself. To do that, you can use the same scripts, used during CI.

To get the user data, you need to have `curl` installed. Navigate to the
project root and run:

```bash
# ./utils/get_matrix_users <homeserver> <session token>
$ ./utils/get_matrix_users 'matrix.org' 'syt_foobarbazz_thisismyaccesstokenn_1ab2c3'
  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                 Dload  Upload   Total   Spent    Left  Speed
100 22.0M    0 22.0M    0     0  2890k      0 --:--:--  0:00:07 --:--:-- 4288k
```

Getting room data is similar:

```bash
# ./utils/get_matrix_rooms <homeserver>
$ ./utils/get_matrix_rooms 'matrix.org'
  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                 Dload  Upload   Total   Spent    Left  Speed
100  2095    0  2095    0     0  16757      0 --:--:-- --:--:-- --:--:-- 16760
```

{{< alert >}}
Downloading user data works on any homeserver. Downloading rooms only on
homeservers, which share **their** public rooms via federation, such as
`matrix.org`.
{{< /alert >}}

## Parameters

The `identifier` shortcode has the following parameters:

| Parameter | Description                                                      |
| --------- | ---------------------------------------------------------------- |
| `[0]`     | The public Matrix room/space/user identifier or an email address |

## Examples

```md
{{</* identifier "#python:matrix.org" */>}}
{{</* identifier "@michael:michaelsasser.org" */>}}
{{</* identifier "user@domain.tld" */>}}
```

<!-- prettier-ignore-start -->
{{< rendered >}}
{{< identifier "#python:matrix.org" >}}
{{< identifier "@michael:michaelsasser.org" >}}
{{< identifier "user@domain.tld" >}}
{{< /rendered >}}
<!-- prettier-ignore-end -->

## Code

Below you find the implementation of the shortcode.

### HTML

Defined in `layouts/shortcodes/identifier.html`.

{{< alert preset="license-code" >}}{{< /alert >}}

{{% readfile path="/layouts/shortcodes/identifier.html" %}}

#### Download

Copy the source code above or use the download link below to use this file on
your website according to the license.

{{< download title="Identifier Shortcode" path="/layouts/shortcodes/identifier.html" >}}

### SASS

Defined in: `/assets/scss/components/_shortcode_identifier.scss`

{{< alert preset="license-code" >}}{{< /alert >}}

{{% readfile path="/assets/scss/components/_shortcode_identifier.scss" %}}

#### Download

Copy the source code above or use the download link below to use this file on
your website according to the license.

{{< download title="Identifier Style" path="/assets/scss/components/_shortcode_identifier.scss" >}}

### CI/CD Scripts

The following scripts are run during continuous integration and continuous
delivery to create the data used by the website to show the room/user
logos/avatars.

#### Get Matrix Rooms

Defined in: `/utils/get_matrix_rooms`

{{< alert preset="license-code" >}}{{< /alert >}}

{{% readfile path="/utils/get_matrix_rooms" %}}

#### Download

Copy the source code above or use the download link below to use this file on
your website according to the license.

{{< download title="Get Matrix Rooms CI/CD Script" path="/utils/get_matrix_rooms" >}}

#### Get Matrix Users

Defined in: `/utils/get_matrix_users`

{{< alert preset="license-code" >}}{{< /alert >}}

{{% readfile path="/utils/get_matrix_users" %}}

#### Download

Copy the source code above or use the download link below to use this file on
your website according to the license.

{{< download title="Get Matrix Users CI/CD Script" path="/utils/get_matrix_users" >}}
