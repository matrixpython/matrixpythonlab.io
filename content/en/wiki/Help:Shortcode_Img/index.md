---
title: "Help:Shortcode Img"
url: "/wiki/Help\\:Shortcode_Img"
description: ""
lead: "Display zoomable images, optionally in a figure."
date: 2022-07-17T06:19:08+02:00
lastmod: 2022-07-17T06:19:08+02:00
contributors: ["Michael Sasser"]
wiki_categories: ["help", "contribute", "shortcode"]
draft: false
images: ["image.svg"]
weight: 50
toc: true

infobox:
  header: Img
  data:
    - type: subheader
      header: '[Shortcode]({{< ref "/wiki_categories/shortcode" >}})'
    - type: header
      header: Description
    - type: label
      label: Type
      value: HTML Shortcode
    - type: label
      label: Nested?
      value: No
    - type: label
      label: Shortcode
      value: "`img`"
    - type: label
      label: Return Type
      value: HTML
    - type: fulldata
      label: Short Description
      value: |
        Display an image, which is zoomable by default, optionally in an
        HTML figure.
    - type: header
      header: Development
    - type: label
      label: Maintainer
      value: '[Michael Sasser]({{< ref "michael-sasser" >}})'
---

It is possible to use the file formats `jpeg`, `png`, `tiff`, `bmp`, `gif` and
`svg`. Images are lazyloaded, blurred up, and responsive. They need to be place
in a [page bundle](https://gohugo.io/content-management/page-bundles/), for
example, like
[this site bundle](https://gitlab.com/matrixpython/website/-/blob/main/content/en/docs/tutorial/create_account):

```bash
# tree of ./content/en/docs/tutorial/create_account
...
├── tutorial/
│   ├── create_account/
│   │   ├── index.md
│   │   ├── element-create-account-recaptcha.png
│   │   ├── element-create-account-accept-terms.png
│   │   ├── element-create-account-email.png
│   │   ├── element-create-account-filled.png
│   │   ├── element-create-account-github-sso.png
│   │   ├── element-create-account-google-sso.png
│   │   ├── element-create-account-registration-successful.png
│   │   ├── element-create-account-verification-mail.png
│   │   ├── element-create-account.png
│   │   ├── element-initial-view.png
│   │   ├── element-sign-in-filled.png
│   │   ├── element-sign-in.png
│   │   └── element-welcome.png
│   └── _index.md
└── _index.md
```

## Parameters

The `img` shortcode has the following parameters:

| Parameter      | Optional | Description                                |
| -------------- | -------- | ------------------------------------------ |
| `src`          | No       | The relative path to the image             |
| `width`        | Yes      | The width of the image                     |
| `height`       | Yes      | The height of the image                    |
| `figure_class` | Yes      | Add classes to the figure                  |
| `img_class`    | Yes      | Add classes to the HTML `img`              |
| `alt`          | Yes      | Add an alt string to the HTML `img`        |
| `caption`      | Yes      | Add an figure to the image with an caption |

## Examples

{{< alert title="Front Matter" >}}
Remember to add every image you use to the `images` variable in the document
header. <br /> For example: `images: ["foo.svg", "bar.jpeg"]`
{{< /alert >}}

```md
{{</* img src="image.svg" width="50" */>}}
{{</* img src="image.svg" width="100" alt="Our logo" caption="<em>The Matrix+Python Logo</em>" */>}}
{{</* img src="image.svg" */>}}
```

<!-- prettier-ignore-start -->
{{< rendered >}}
{{< img src="image.svg" width="50" >}}
{{< img src="image.svg" width="100" alt="Our logo" caption="<em>The Matrix+Python Logo</em>" >}}
{{< img src="image.svg" >}}
{{< /rendered >}}
<!-- prettier-ignore-end -->

## Code

Below you find the implementation of the shortcode.

### HTML

Defined in `layouts/shortcodes/img.html`.

{{< alert preset="license-code" >}}{{< /alert >}}

{{% readfile path="/layouts/shortcodes/img.html" %}}

#### Download

Copy the source code above or use the download link below to use this file on
your website according to the license.

{{< download title="Accordion Item Shortcode" path="/layouts/shortcodes/img.html" >}}

### SASS

Defined in: `/assets/scss/components/_shortcode_img.scss`

{{< alert preset="license-code" >}}{{< /alert >}}

{{% readfile path="/assets/scss/components/_shortcode_img.scss" %}}

#### Download

Copy the source code above or use the download link below to use this file on
your website according to the license.

{{< download title="Img Style" path="/assets/scss/components/_shortcode_img.scss" >}}

### JavaScript

Defined in: `/assets/js/medium-zoom.js` <br />
Uses: `medium-zoom` (Package: [NPM](https://www.npmjs.com/package/medium-zoom), License: [MIT](https://github.com/francoischalifour/medium-zoom/blob/master/LICENSE))

{{< alert preset="license-code" >}}{{< /alert >}}

{{% readfile path="/assets/js/medium-zoom.js" %}}

#### Download

Copy the source code above or use the download link below to use this file on
your website according to the license.

{{< download title="Medium-Zoom Script" path="/assets/js/medium-zoom.js" >}}
