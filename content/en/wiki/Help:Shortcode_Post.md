---
title: "Help:Shortcode Post"
url: "/wiki/Help\\:Shortcode_Post"
description: ""
lead: "Display a blog post in a card."
date: 2022-07-17T06:19:08+02:00
lastmod: 2022-07-17T06:19:08+02:00
contributors: ["Michael Sasser"]
wiki_categories: ["help", "contribute", "shortcode"]
draft: false
images: []
weight: 50
toc: true

infobox:
  header: Post
  data:
    - type: subheader
      header: '[Shortcode]({{< ref "/wiki_categories/shortcode" >}})'
    - type: header
      header: Description
    - type: label
      label: Type
      value: HTML Shortcode
    - type: label
      label: Nested?
      value: No
    - type: label
      label: Shortcode
      value: "`post`"
    - type: label
      label: Return Type
      value: HTML
    - type: fulldata
      label: Short Description
      value: |
        Display a post in a card, optionally linked to the original blog post.
    - type: header
      header: Development
    - type: label
      label: Maintainer
      value: '[Michael Sasser]({{< ref "michael-sasser" >}})'
---

Render a `card` of a blog post. This shortcode cannot be nested. If it somehow
finds multiple blog posts with the same title, it will render only the oldest
post.

## Parameters

The post shortcode has the following parameters:

| Parameter   | Description                                                            |
| ----------- | ---------------------------------------------------------------------- |
| `title`     | The title of the post to be shown                                      |
| `linked`    | Adds the link to the card to the post, if `linked` is not `false`      |
| `container` | Renders a `class="container"` around it, if `container` is not `false` |
| `mt`        | The top margin used in a bootstrap class                               |
| `mp`        | The top padding used in a bootstrap class                              |

{{< alert title="Note" >}} In this shortcodes the <code>false</code> value is
a string, not a boolean. Everything else, even <code>nil</code>, will render
them. {{< /alert >}}

## Examples

```md
{{</* post title="Spaces Announcement 🎉" linked="false" container="false" */>}}
```

<!-- prettier-ignore-start -->
{{< rendered >}}
{{< post title="Spaces Announcement 🎉" linked="false" container="false" >}}
{{< /rendered >}}
<!-- prettier-ignore-end -->

## Code

Below you find the implementation of the shortcode.

### HTML

Defined in `layouts/shortcodes/post.html`.

{{< alert preset="license-code" >}}{{< /alert >}}

{{% readfile path="/layouts/shortcodes/post.html" %}}

#### Download

Copy the source code above or use the download link below to use this file on
your website according to the
license.

{{< download title="Post Shortcode" path="/layouts/shortcodes/post.html" >}}

### SASS

Defined in: `/assets/scss/components/_shortcode_post.scss`

{{< alert preset="license-code" >}}{{< /alert >}}

{{% readfile path="/assets/scss/components/_shortcode_post.scss" %}}

#### Download

Copy the source code above or use the download link below to use this file on
your website according to the
license.

{{< download title="Post Style" path="/assets/scss/components/_shortcode_post.scss" >}}
