---
title: "Help:Shortcode Reporef"
url: "/wiki/Help\\:Shortcode_Reporef"
description: ""
lead: "Create a string, which can be used as link to the repo for a local path."
date: 2022-08-11T09:58:00+02:00
lastmod: 2022-08-11T09:58:00+02:00
contributors: ["Michael Sasser"]
wiki_categories: ["help", "contribute", "shortcode"]
draft: false
images: []
weight: 50
toc: true

infobox:
  header: Reporef
  data:
    - type: subheader
      header: '[Shortcode]({{< ref "/wiki_categories/shortcode" >}})'
    - type: header
      header: Description
    - type: label
      label: Type
      value: HTML Shortcode
    - type: label
      label: Nested?
      value: No
    - type: label
      label: Shortcode
      value: "`reporef`"
    - type: label
      label: Return Type
      value: HTML
    - type: fulldata
      label: Short Description
      value: |
        Create a string, which can be used as link to the repo for a local path
    - type: header
      header: Development
    - type: label
      label: Maintainer
      value: '[Michael Sasser]({{< ref "michael-sasser" >}})'
---

This shortcode works exactly like the `ref` shortcode, but the resulting link
points to the repo instead.

For more information on the `ref` shortcode, check out the shortcodes docs.

{{< ln "wiki/Help:Shortcode_Ref" >}}

## Parameters

The details shortcode has the following parameters:

| Parameter | Description                                                                                                                                                                                       |
| --------- | ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| `[0]`     | The path to a page, with or without a file extension, with or without an anchor. A path without a leading `/` is first resolved relative to the given context, then to the remainder of the site. |

## Examples

```md
{{</* reporef "wiki/Help:Shortcode_Ln" */>}}
```

<!-- prettier-ignore-start -->
{{< rendered >}}
{{< reporef "wiki/Help:Shortcode_Ln" >}}
{{< /rendered >}}
<!-- prettier-ignore-end -->

## Code

Below you find the implementation of the shortcode.

### HTML

Defined in `layouts/shortcodes/reporef.html`.

{{< alert preset="license-code" >}}{{< /alert >}}

{{% readfile path="/layouts/shortcodes/reporef.html" %}}

#### Download

Copy the source code above or use the download link below to use this file on
your website according to the license.

{{< download title="Details Shortcode" path="/layouts/shortcodes/reporef.html" >}}
