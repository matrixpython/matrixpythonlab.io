---
title: "Help:Shortcode Reporting Issues"
url: "/wiki/Help\\:Shortcode_Reporting_Issues"
description: ""
lead: "Display content, which shows the user how to report issues."
date: 2022-07-17T06:19:08+02:00
lastmod: 2022-07-17T06:19:08+02:00
contributors: ["Michael Sasser"]
wiki_categories: ["help", "contribute", "shortcode"]
draft: false
images: []
weight: 50
toc: true

infobox:
  header: Reporting Issues
  data:
    - type: subheader
      header: '[Shortcode]({{< ref "/wiki_categories/shortcode" >}})'
    - type: header
      header: Description
    - type: label
      label: Type
      value: HTML Shortcode
    - type: label
      label: Nested?
      value: No
    - type: label
      label: Shortcode
      value: "`reporting_issues`"
    - type: label
      label: Return Type
      value: HTML
    - type: fulldata
      label: Short Description
      value: |
        Let users know, how to report issues.
    - type: header
      header: Development
    - type: label
      label: Maintainer
      value: '[Michael Sasser]({{< ref "michael-sasser" >}})'
---

This shortcode will render the "Reporting Issues" section of the Code of
Conduct, which is used in many places on this website.<br />

## Parameters

The `report_issues` shortcode does not have any parameters.

## Examples

```md
{{</* reporting_issues */>}}
```

<!-- prettier-ignore-start -->
{{< rendered >}}
{{< reporting_issues >}}
{{< /rendered >}}
<!-- prettier-ignore-end -->

## Code

Below you find the implementation of the shortcode.

### HTML

Defined in `layouts/shortcodes/reporting_issues.html`.

{{< alert preset="license-code" >}}{{< /alert >}}

{{% readfile path="/layouts/shortcodes/reporting_issues.html" %}}

#### Download

Copy the source code above or use the download link below to use this file on
your website according to the license.

{{< download title="Reporting Issues Shortcode" path="/layouts/shortcodes/reporting_issues.html" >}}

### SASS

Defined in: `/assets/scss/components/_shortcode_identifier.scss`

{{< alert preset="license-code" >}}{{< /alert >}}

This shortcode uses the styles of the `identifier` shortcode, as it is derived
from that.

{{< ln "wiki/Help:Shortcode_Identifier" >}}

{{% readfile path="/assets/scss/components/_shortcode_identifier.scss" %}}

#### Download

Copy the source code above or use the download link below to use this file on
your website according to the license.

{{< download title="Identifier Style" path="/assets/scss/components/_shortcode_identifier.scss" >}}
