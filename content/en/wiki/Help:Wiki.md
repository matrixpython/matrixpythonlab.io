---
title: "Help:Wiki"
url: "/wiki/Help\\:Wiki"
description: ""
lead: "Create a wiki page."
date: 2022-07-17T06:19:08+02:00
lastmod: 2022-07-17T06:19:08+02:00
contributors: ["Michael Sasser"]
wiki_categories: ["help", "contribute", "layout"]
draft: false
images: []
weight: 50
toc: true
---

Our wiki is collaboratively edited by the users. It is meant as a platform to
collect information about our community, the Python programming language in
general and useful information. It consists of categorized pages, loosely
linked together.

## Location

| Directory                                            | Description                    |
| ---------------------------------------------------- | ------------------------------ |
| [content/\<Language\>/wiki/]({{< reporef "wiki" >}}) | The base directory of the wiki |

{{% newpage name="wiki" %}}

## Front Matter

{{< variable_structure "Wiki" "title" "url" "description" "lead" "date" "lastmod" "draft" "weight" "images" "contributors" >}}

Notice, the `weight` in wiki pages is always `50`.

## Code

Below you find the implementation of the layout.

### HTML

#### Single Page Layout

Defined in `layouts/wiki/single.html`.

{{< alert preset="license-code" >}}{{< /alert >}}

{{% readfile path="/layouts/wiki/single.html" %}}

#### List Page Layout

Defined in `layouts/wiki/list.html`.

{{< alert preset="license-code" >}}{{< /alert >}}

{{% readfile path="/layouts/wiki/list.html" %}}

### Archetype

Defined in `layouts/wiki.md`.

{{< alert preset="license-code" >}}{{< /alert >}}

{{% readfile path="/archetypes/wiki.md" type="yaml" %}}
