---
title: "Learn Python"
url: "/wiki/Learn_Python/"
description: "Find out, how to learn Python"
lead: "Find out, how to learn Python"
date: 2022-11-17T12:10:41+01:00
lastmod: 2022-11-17T12:10:41+01:00
contributors: []
draft: false
images: []
weight: 50
toc: true
---

One of the most frequently asked question in our Community, is how to learn
Python? To answer this question, we have curated a list of books, websites
and courses which many of us used to get started with Python. But we are not
the only ones who have done this. There is also the official
[Beginners Guide](https://wiki.python.org/moin/BeginnersGuide)
which features many options to choose from and the
[Non Programmers](https://wiki.python.org/moin/BeginnersGuide/NonProgrammers)
guide in the Python wiki, you should know about.

If Python is your first programming language and if you just start out, the
[Python documentation](https://docs.python.org/3/) itself might be a little
hard to read or understand at first but over time, this will become your
most important source of information about it. You should definitely check it
out from time to time, when you feel more comfortable with the language.

## Books

### Automate the Boring Stuff with Python

{{< badge title="Free Online" type="danger" shape="square" >}}
{{< badge title="Paid Book" type="primary" shape="square" >}}

<div class="text-center pb-4">
  <!-- TODO: Use the cover as image; Ask for using it -->
  <!-- <img src="https://automatetheboringstuff.com/images/cover_automate2_thumb.jpg"></img> -->
</div>

"Automate the Boring Stuff with Python" by Al Sweigart et al. is the most
recommended books in the Community. It focuses on people who have never written
any line of code and teaches them the basics of Python.

<br /><br />

Website: [https://automatetheboringstuff.com](https://automatetheboringstuff.com/)

Read for free: [https://automatetheboringstuff.com/#toc](https://automatetheboringstuff.com/)

ISBN-13: 978-1-59327-992-9

## Websites

### Python Tutorial

{{< badge title="Free Online" type="danger" shape="square" >}}

<br />

The [Python Tutorial](https://python-course.eu/python-tutorial/) by
[Bernd Klein](https://www.bklein.de/) was created in 2010 around the idea to
take a different approach in teaching python. He uses comprehensible examples
and illustrates them with various graphics.

<br /><br />

Website: [https://python-course.eu](https://python-course.eu)

## Videos

### YouTube: Corey Schafer's Python Tutorials

{{< badge title="Free Course" type="danger" shape="square" >}}

<br /><br />

Corey Schafer created well over 100, free to watch, videos about Python,
dedicated to newcomers.
In addition they feature many free courses for `Pandas`, `Matplotlib`, `Flask`,
`Django`, even `git` and `SQL`, and many more.

<br /><br />

YouTube Channel: [https://www.youtube.com/c/Coreyms/featured](https://www.youtube.com/c/Coreyms/featured)

## Courses

### CS50P - Introduction to Programming with Python

{{< badge title="OpenCourseWare" type="danger" shape="square" >}}

<br /><br />

The course
[CS50P - Introduction to Programming with Python](https://cs50.harvard.edu/python/2022/)
by [David J. Malan](https://cs.harvard.edu/malan/) of the Harvard University
was created with the main focus on its students. Nevertheless, he made it
available for everyone else too, as OpenCourseWare. He also encourages teachers
to use his materials in class. You get the full videos, slides, notes,
and problem sets for you to work through. The course itself is part of a series
of courses that might also interest you later on. For example:

- [CS50x - Introduction to Computer Science](https://cs50.harvard.edu/x/2023/) (OpenCourseWare; not Python exclusive) by [David J. Malan](https://cs.harvard.edu/malan/); Harvard University
- [CS50w - Web Programming with Python and JavaScript](https://cs50.harvard.edu/web/2020/) (OpenCourseWare) by [Brian Yu](https://brianyu.me/); Harvard University
- [CS50AI - Introduction to Artificial Intelligence with Python](https://cs50.harvard.edu/ai/2020/) (OpenCourseWare) by [Brian Yu](https://brianyu.me/); Harvard University

<br /><br />

Website: [https://cs50.harvard.edu/python/2022/](https://cs50.harvard.edu/python/2022/)

## Talks and Conferences

If you already know the basics of Python you might be interested in checking
out some talks about Python and it's ecosystem:

- [PyCon](https://www.youtube.com/@PyConUS)
- [EuroPython](https://www.youtube.com/EuroPythonConference)

## Other Courses

- [Introduction to SQL](https://sqlbolt.com/) (SQL)
