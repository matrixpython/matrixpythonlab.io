---
title: "Special:Changes"
url: "/wiki/Special\\:Changes/"
description: "A list of recent changes"
lead: "A list of recent changes"
date: 2022-06-06T09:40:00+20:00
lastmod: 2022-06-06T09:40:00+20:00
contributors: ["Michael Sasser"]
wiki_categories: ["special"]
draft: false
images: []
weight: 100
toc: true
menu:
  wiki:
    name: Changes
    parent: wiki_tools
    weight: 10
  wiki_categories:
    name: Changes
    parent: wiki_categories_tools
    weight: 10
---

{{< alert preset="todo" >}}{{< /alert >}}

## GitLab

All changes made to this website, including our wiki, are done using `git` and
[GitLab](https://gitlab.com/). You find all changes in our
[our repository](https://gitlab.com/matrixpython/website).
