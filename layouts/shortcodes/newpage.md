<!--

  Newpage Shortcode
  =================

  Contributors: Michael Sasser
  Maintainer: Michael Sasser
  License: MIT
  Version: 1
  Child Shortcodes: None
  Parent Shortcodes: None

  LAYOUT: /layouts/shortcodes/newpage.md
  DOCS:   /content/<language>/wiki/Help:Shortcode_Newpage.md

  Description
  -----------

  Use the download to show the user how to create a new page in a specific
  section of the website.

  Changelog
  =========

  Version 1 (2022-07-22)
  ----------------------

  Initial release

-->

{{ $name := .Get "name" }}
{{ $by_year := .Get "by_year" | default false }}
{{ $by_section := .Get "by_section" | default false }}

{{ $path_package := "package.json" }}
{{ $data_package := dict }}
{{ $path := $path_package }}
{{ with resources.Get $path_package }}
{{ with . | transform.Unmarshal }}
{{ $data_package = . }}
{{ end }}
{{ else }}
{{ errorf "Unable to get global resource %q" $path_package }}
{{ end }}

## {{ i18n "new-page-title" | title }} {{ $name | title }} {{ i18n "page" | title }}

{{ i18n "new-page-to-create-page-use-command" }} `npm run create -- '{{ $name | lower }}/{{- if $by_year -}}<year>/{{- end -}}{{- if $by_section -}}<section>/{{- end -}}<pageName>.md'`.
{{ i18n "new-page-after-that-edit" }} `content/{{ .Page.Lang }}/{{ $name | lower }}/{{- if $by_year -}}<year>/{{- end -}}{{- if $by_section -}}<section>/{{- end -}}<pageName>.md`.

### {{ i18n "example" | title }}

{{ i18n "new-page-to-create-page" }} `foo`:

```sh
$ npm run create -- '{{ $name | lower }}/{{- if $by_year -}}{{- now.Format "2006" -}}/{{- end -}}{{- if $by_section -}}tutorial/{{- end -}}foo.md'

> {{- index $data_package "name" | markdownify -}} @ {{- index $data_package "version" | markdownify }} create
> exec-bin node_modules/.bin/hugo/hugo new "{{ $name | lower }}/{{- if $by_year -}}{{- now.Format "2006" -}}/{{- end -}}{{- if $by_section -}}tutorial/{{- end -}}foo.md"

Content "/home/username/path_to_repository/repository_root_directory/content/{{ .Page.Lang }}/{{ $name | lower }}/{{- if $by_year -}}{{- now.Format "2006" -}}/{{- end -}}{{- if $by_section -}}tutorial/{{- end -}}foo.md" created
```
